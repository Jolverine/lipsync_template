#!/bin/bash

# Inputs: (1) text file in kaldi format (2) output file (3) min length (number of words) of each phrase (4) phrase_list_<lang> of that particular language
# Example: bash phrase_text_v2.sh text text_out 2 hindi

infile=$1
outfile=$2
minlen=$3
lang=$4

list=$PWD"/phrase_breaks/phrase_list_"$lang

cp $infile text_temp
less $list | parallel -j1 -k "sed -i 's: {1} : {1}, :g' text_temp"

awk -v minLen=$minlen '{var1=$1;$1="";var2=1;var3=0;split($0, chars, "");printf("\n%s-%02d",var1,var2);for (i=1; i <= length($0); i++) {if(chars[i]==" "){var3=var3+1}; if(chars[i]==",") {if(var3>minLen){var3=0;var2=var2+1;printf("\n%s-%02d",var1,var2)}} else {printf("%s", chars[i])}}}' text_temp > $outfile

sed -i '1d' $outfile
#rm text_temp

# Post synthesis for obtaining concatenated wav files:
# for f in $(ls wav/| cut -d "-" -f1 | sort | uniq); do sox wav/$f-*.wav wav_concatenated/$f.wav; done
