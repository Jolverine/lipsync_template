import sys,os
import ast

class concat_syl(object):
	def __init__(self,num_syl,transcriptiontxt, lab_folder,target_location):
		self.num_syl=num_syl
		self.transcription=transcriptiontxt
		self.lab_folder=lab_folder
		self.target_location=target_location
		
	def main(self):
		num_syl = self.num_syl
		lab_folder = self.lab_folder
		transcription = self.transcription
		target_location=self.target_location
		dictionary = {}
		with open(num_syl) as f:
			for line in f:
			    (key, val) = line.split()
			    dictionary[str(key)] = val

		lines =[]
		with open(transcription ,'r') as f:
			lines = f.read().splitlines()
		for i in range(len(lines)):
		    lines[i] = lines[i].split(" ", 1)[1]
		   
		for line,file in zip(lines,sorted(os.listdir(lab_folder))):
			lab_lines=[]
			with open(lab_folder+"/"+file ,'r') as fh:
				full_lab_lines = fh.read().splitlines()
				lab_lines = full_lab_lines[1:]
			for i in range(len(lab_lines)):
				lab_lines[i] = lab_lines[i].split(" ")
			lab_lines.insert(0, ['0.0000', '125', 'SIL', '1'])
			lab_lines.append([lab_lines[-1][0], '125', 'SIL', '1'])
			words=line.split()
			current=1
			index=0
			for word in words:
				numofsyls=int(dictionary[word])
				starting_timestamp = lab_lines[current-1][0]
				label = ""
				while (numofsyls>0):
					label = label + lab_lines[current][2]
					numofsyls-=1
					if(numofsyls>0):
						current+=1
				end_timestamp=lab_lines[current][0]
				with open(target_location+"/"+file,'a') as fh:
					fh.write(" ".join([str(starting_timestamp),str(end_timestamp),str(label),"\n"]))
				current+=1
				if((lab_lines[current][2]=="SIL" or lab_lines[current][2]=="sp") and index != len(words)-1):
					with open(target_location+"/"+file,'a') as fh:
						fh.write(" ".join([str(lab_lines[current-1][0]),str(lab_lines[current][0]),str(lab_lines[current][2]),"\n"]))
					current+=1
				index+=1
	if __name__ == "__main__":
		main()

