#!/bin/sh

#  splitAlignments.py
#  
#
#  Created by Mano Ranjith Kumar on 9/3/21.
#
#
#

from pathlib import Path
from shutil import copyfile
import sys,csv
import os

class corr_with_ene(object):
	def __init__(self,ctm_file,ste_file,target_dir):
	    self.ctm_file = ctm_file
	    self.ste_file = ste_file
	    self.target_dir = target_dir
	    	
	def main(self):
		ctm_file = self.ctm_file
		ste_file = self.ste_file
		mylabfile = Path(ste_file)
		target_dir = self.target_dir
		Path(target_dir).mkdir(parents=True, exist_ok=True)
		if mylabfile.is_file():
			print("Fonund ste file correcting boundaries"+ste_file)
		else:
			print("Ste file named "+ ste_file +" is missing")
		with open(ste_file) as f:
			ste_values = f.read().splitlines()
		size = os.path.getsize(ctm_file)       
		with open(ctm_file,"r") as fp:
			line = fp.readline()
			size -= len(line)
			num_lines = sum(1 for line in open(ctm_file))
			if(num_lines == 1):
				line = line.strip()
				columns = line.split()
				copyfile(ctm_file,target_dir+'/'+columns[0]+'.stxt')
				sys.exit()
			curr_start_value = -1
			curr_label = ""
			curr_duration = 0
			written_flag = 0
			filename = ""
			final = []
			while line:
				line = line.strip()
				columns = line.split()
				start_value = float(columns[2])
				label = str(columns[4])
				duration = float(columns[3])
				end_value = float(columns[3]) + float(columns[2])
				index = int(end_value * 22050)-1
				filename=columns[0]
		       #print(index)
		       #print(ste_values)
				if(index >= len(ste_values)):
					index = len(ste_values)-1
				ene_value = float(ste_values[index])
				if(ene_value > 0.05):
					if(curr_start_value == -1):
						curr_start_value = start_value
					curr_label  = curr_label + label
					curr_duration = curr_duration + duration
				else:
					#sstep = open("stats_for_ste", "a+")
					#sstep.write("1"+",") #providng stats
					#sstep.close()
					with open(target_dir+'/'+columns[0]+'.stxt', 'a+') as the_file:
						if(curr_label != "" and curr_start_value != -1 and curr_duration != 0):
							written_flag=1
							columns[2] = curr_start_value
							columns[3] = curr_duration + duration
							columns[3] = columns[3]+(start_value-(curr_start_value+curr_duration)) 
							columns[4] = curr_label + label
							curr_start_value = -1
							curr_label  = ""
							curr_duration = 0
						line = " ".join([str(i) for i in columns])
						the_file.write(line+"\n")
					if(not size and written_flag==0 and not Path(target_dir+'/'+columns[0]+'.stxt').is_file()):
						with open(target_dir+'/'+columns[0]+'.stxt', 'a+') as the_file:
							columns[2] = curr_start_value
							columns[3] = curr_duration
							columns[4] = curr_label
							curr_start_value = -1
							curr_label  = ""
							curr_duration = 0
							line = " ".join([str(i) for i in columns])
							the_file.write(line+"\n")
				line = fp.readline()
				size -= len(line)
		       
			if( curr_start_value != -1 or curr_label != "" or curr_duration != 0):
				new_columns=[]
				new_columns.append(filename)
				new_columns.append(1)   
				with open(target_dir+'/'+new_columns[0]+'.stxt', 'a+') as the_file:
					new_columns.append(curr_start_value)
					new_value = curr_duration+duration
					new_value = new_value+(start_value-(curr_start_value+curr_duration)) 
					new_columns.append(new_value)
					new_columns.append(curr_label)
					#print(curr_label)
					curr_start_value = -1
					curr_label  = ""
					curr_duration = 0
					line = " ".join([str(i) for i in new_columns])
					the_file.write(line+"\n")
			#sstep = open("stats_for_ste", "a+")
			#sstep.write("1"+",") #providng stats
			#sstep.close()
