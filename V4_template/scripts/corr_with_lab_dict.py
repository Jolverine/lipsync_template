#!/bin/sh

#  splitAlignments.py
#  
#
#  Created by Mano Ranjith Kumar on 9/3/21.
#
#
#
from pathlib import Path
import sys,csv

class corr_with_lab(object):
	def __init__(self,ctm_file,lab_file,target_dir):
		self.ctm_file = ctm_file
		self.lab_file = lab_file
		self.target_dir = target_dir

	def main(self): 
		last_word = 0    
		missing = 0
		ctm_file = self.ctm_file
		lab_file = self.lab_file
		mylabfile = Path(lab_file)
		target_dir = self.target_dir
		Path(target_dir).mkdir(parents=True, exist_ok=True)
		if mylabfile.is_file():
			print("Fonund lab file correcting boundaries")
		else:
			print("Lab file named "+ lab_file +" is missing")
		start_lab_values =[]
		end_lab_values =[]
		indices_before_sil =[]
		index=0
		with open(lab_file,"r") as lp:	
			line = lp.readline()
			while line:
				line = line.strip()
				columns = line.split()
				if(str(columns[2])=="SIL" or str(columns[2])=="sp"):
					indices_before_sil.append(index-1)
					index+=1
					line = lp.readline()
					continue
				start_lab_values.append(float(columns[0]))
				end_lab_values.append(float(columns[1]))
				line = lp.readline()
				index+=1
		lines = []       
		with open(ctm_file,"r") as fp:
			lines = fp.read().splitlines()
		for i in range(len(lines)):
			line = lines[i]
			line = line.strip()
			columns = line.split()
			start_value = float(columns[2])
			if(i in indices_before_sil):
				if(columns[4].endswith("*")):
					columns[4] = columns[4].replace("*", "<B>")
				elif(not columns[4].endswith("<B>")):
					columns[4] = str(columns[4])+"<B>"
			#if(abs(start_value - start_lab_values[i]) <= 0.150):
			columns[2] = start_lab_values[i]
			columns[3] = float(columns[3])-float(columns[2])
			end_value = float(columns[3]) + float(columns[2])
			#if(abs(end_value - end_lab_values[i] <= 0.150)):
			columns[3] = end_lab_values[i] - float(columns[2])
			
			with open(target_dir+'/'+columns[0]+'.gtxt', 'a+') as the_file:
				line = " ".join([str(i) for i in columns])
				the_file.write(line+"\n")
       
