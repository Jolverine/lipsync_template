#!/bin/sh

#  splitAlignments.py
#  
#
#  Created by Mano Ranjith Kumar on 9/3/21.
#
#
#
from pathlib import Path
import sys,csv
results=[]
ctm_file= sys.argv[1]
target_dir = sys.argv[2]
Path(target_dir).mkdir(parents=True, exist_ok=True)
with open(ctm_file) as fp:
   line = fp.readline()
   cnt = 1
   current_column =""
   while line:
       line = line.strip()
       columns = line.split()
       with open(target_dir+'/'+columns[0]+'.txt', 'a+') as the_file:
         the_file.write(line+"\n")
       line = fp.readline()
       cnt += 1
