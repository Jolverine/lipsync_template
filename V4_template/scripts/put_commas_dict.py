import sys

class put_commas(object):
	def __init__(self,transcript_with_commas,target_ctm):
		self.transcript_with_commas = transcript_with_commas
		self.target_ctm = target_ctm
	
	def main(self):
		transcript_with_commas = self.transcript_with_commas
		target_ctm = self.target_ctm
		with open(transcript_with_commas,'r') as f:
			lines = f.readlines()
		with open(target_ctm,'r') as fh:
			ctm_lines = fh.read().splitlines()
			
		counter =0;
		for line in lines:
			words = line.strip().split()
			for i in range(len(words)):
				if(i!=0):
					counter+=1
		if(counter != len(ctm_lines)):
			print(transcript_with_commas)
			print(counter)
			print(len(ctm_lines))
			print(target_ctm)
			sys.exit("The no of words doesn't match with the ctm. Kindly check the punctuations")
		counter = 0
		f.close()
		fh.close()
		with open(transcript_with_commas,'r') as f:
			lines = f.readlines()
		with open(target_ctm,'r') as fh:
			ctm_lines = fh.read().splitlines()		
		for line in lines:
			words = line.strip().split()
			for i in range(len(words)):
				if(i!=0):
					if(words[i].endswith("?") or words[i].endswith("!") or words[i].endswith(",") and i != len(words)-1):
						if(not ctm_lines[counter].strip().endswith("<B>") and not ctm_lines[counter].strip().endswith("*")):
							ctm_lines[counter] = ctm_lines[counter].strip()+"<B>"
					else:
						if(not ctm_lines[counter].strip().endswith("<B>") and not ctm_lines[counter].strip().endswith("*")):
							ctm_lines[counter] = ctm_lines[counter].strip()+"*"
					counter+=1
		with open(target_ctm, "w") as myfile:
		    	myfile.writelines("\n".join(ctm_lines))		
