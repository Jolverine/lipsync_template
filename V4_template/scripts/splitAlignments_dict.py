#!/bin/sh

#  splitAlignments.py
#  
#
#  Created by Mano Ranjith Kumar on 9/3/21.
#
#
#
from pathlib import Path
import sys,csv

class splitAlignments(object):
	def __init__(self,ctm_file,target_dir):
		self.ctm_file = ctm_file
		self.target_dir = target_dir
	
	def main(self):
		results=[]
		ctm_file= self.ctm_file
		target_dir = self.target_dir
		Path(target_dir).mkdir(parents=True, exist_ok=True)
		with open(ctm_file) as fp:
			line = fp.readline()
			cnt = 1
			current_column =""
			while line:
				line = line.strip()
				columns = line.split()
				results.append(str(target_dir+'/'+columns[0]+'.txt'))
				with open(target_dir+'/'+columns[0]+'.txt', 'a+') as the_file:
					the_file.write(line+"\n")
				line = fp.readline()
				cnt += 1
		return results
