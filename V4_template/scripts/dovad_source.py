import sys
from pydub import AudioSegment
from pydub.silence import split_on_silence
from pydub.silence import detect_nonsilent
from pathlib import Path
####################################
# Written by Mano Ranjith Kumar M  #
# Date March 15 		   #
###################################
#Provide mp3 format with arguments filename folder name
#Define a function to normalize a chunk to a target amplitude.

def match_target_amplitude(aChunk, target_dBFS):
    ''' Normalize given audio chunk '''
    change_in_dBFS = target_dBFS - aChunk.dBFS
    return aChunk.apply_gain(change_in_dBFS)
# Load your audio.
mp3_name=sys.argv[1]
dest_folder=sys.argv[2]
Path(dest_folder).mkdir(parents=True, exist_ok=True)
song = AudioSegment.from_mp3(mp3_name)
# Split track where the silence is 2 seconds or more and get chunks using 
# the imported function.
chunks = split_on_silence (
    # Use the loaded audio.
    song, 
    # Specify that a silent chunk must be at least 2 seconds or 2000 ms long.
    min_silence_len = 1000,
    # Consider a chunk silent if it's quieter than -16 dBFS.
    # (You may want to adjust this parameter.)
    silence_thresh = -30
)
#print(chunks[0].__dict__)
# Process each chunk with your parameters
for i, chunk in enumerate(chunks):
    # Create a silence chunk that's 0.5 seconds (or 500 ms) long for padding.
    silence_chunk = AudioSegment.silent(duration=500)

    # Add the padding chunk to beginning and end of the entire chunk.
    audio_chunk = silence_chunk + chunk + silence_chunk

    # Normalize the entire chunk.
    normalized_chunk = match_target_amplitude(audio_chunk, -20.0)

    # Export the audio chunk with new bitrate.
    print("Exporting chunk{0}.mp3.".format(i))
    normalized_chunk.export(
        dest_folder+"/chunk{0}.mp3".format(i),
        bitrate = "192k",
        format = "mp3"
    )
boundaries = detect_nonsilent ( song, min_silence_len=1000, silence_thresh=-30)
with open(dest_folder+'/boundaries.txt', 'w') as f:
    for item in boundaries:
        f.write(str(item[0])+" "+str(item[1])+"\n")
