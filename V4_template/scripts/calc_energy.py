import wave
import numpy as np
from pathlib import Path
import sys,csv

def calEnergy(wave_data) :
    energy = []
    sum = 0
    for i in range(100,len(wave_data)-99,1) :
        square = np.square(wave_data[i-100:i+99])
        sum = np.sum(square)
        energy.append(sum)
    return energy
wav_file = sys.argv[1]
ste_file = sys.argv[2]
f = wave.open(wav_file,"rb")
 # getparams() returns the format information of all WAV files at once
params = f.getparams()
# nframes Number of sampling points
nchannels, sampwidth, framerate, nframes = params[:4]
# readframes() Read data according to the sampling point
str_data = f.readframes(nframes) # str_data is a binary string
# Above can be written directly as str_data = f.readframes(f.getnframes())
# Convert to a two-byte array form (two bytes per sample point)
wave_data = np.fromstring(str_data, dtype = np.short)
intial_zeropad = np.zeros(100,dtype=float)
final_zeropad = np.zeros(99,dtype=float)
padded_wave_data = np.concatenate((intial_zeropad,wave_data), axis=0)
padded_wave_data = np.concatenate((padded_wave_data,final_zeropad),axis=0)
#print(padded_wave_data.shape)
#print( "Number of sample points:" + str(len(wave_data))) #output should be the number of sample points
#print(len(padded_wave_data))
f.close()
energy=calEnergy(padded_wave_data)
#print(len(energy))
#print(len(wave_data))
#print(nchannels, sampwidth, framerate, nframes)
amax = max(energy)
amin = min(energy)
norm = [(float(i)-amin)/(amax-amin) for i in energy]

with open(ste_file, 'w') as f:
    for item in norm:
        f.write("%f\n" % item)
