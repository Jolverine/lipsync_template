#!/bin/sh

#  splitAlignments.py
#  
#
#  Created by Mano Ranjith Kumar on 9/3/21.
#
#
#
from pathlib import Path
import sys,csv

def closest(lst, K):
      
    return lst[min(range(len(lst)), key = lambda i: abs(lst[i]-K))]
    
results=[]
ctm_file= sys.argv[1]
gd_file = sys.argv[2]
threshold = 0.100
target_dir = "./temp_data/tts_gd_boundary_files"
Path(target_dir).mkdir(parents=True, exist_ok=True)
with open(ctm_file) as fp:
     gp_array=[]
     sil_array=[]
     sil_counter = 0
     with open(gd_file) as gp:
        gp_line = gp.readline()
        gp_line = gp.readline()
        while gp_line:
             print(gp_line)
             gp_columns = gp_line.split()
             if(str(gp_columns[2]) == "SIL"):
                 sil_array[sil_counter][0] = prev_gp_value
                 sil_array[sil_counter][1] = float(gp_columns[0])-prev_gp_value
                 sil_counter = sil_counter + 1
             gp_array.append(float(gp_columns[0]))
             prev_gp_value = float(gp_columns[0])
             gp_line = gp.readline()
     line = fp.readline()
     cnt = 1
     current_sil = 0
     while line:
         line = line.strip()
         columns = line.split()
         filename = columns[0]
         if(abs(float(columns[2]) - float(closest(gp_array,float(columns[2]))))<= threshold):
         	columns[2] = str(float(closest(gp_array,float(columns[2]))))
         if(abs(float(float(columns[2])+float(columns[3])) - float(closest(gp_array,float(columns[2])+float(columns[3])))) <= threshold):
         	columns[3] = str(float(closest(gp_array,float(columns[2])+float(columns[3]))) - float(columns[2]))
         with open(target_dir+'/'+columns[0]+'.txt', 'a+') as the_file:
           new_line = " ".join(columns)
           the_file.write(new_line+"\n")
         line = fp.readline()
         cnt += 1
     with open(target_dir+'/'+filename+'.txt') as add_sil:
     	 lines = add_sil.readlines()
     	 for i in range(lines):
     	   if(i+1 <= len(lines)):
     	      line1 = lines[i]
     	      line2 = lines[i+1]
     	      columns1 = line1.split()
     	      columns2 = line2.split()
     	      if(float(columns1[2]) < sil_array[current_sil] && float(columns2[2])> sil_array[current_sil]):
     	         ncol = columns1
     	         ncols[2] = sil_array[current_sil][0]
     	         ncols[3] = sil_array[current_sil][1]
     	         new_line = " ".join(ncols)
     	         beforeElement = line2
     	         index = lines.index(beforeElement) 
     	         lines.insert(index, nline) 
     	 line = line.strip()
         columns = line.split()
     	 
