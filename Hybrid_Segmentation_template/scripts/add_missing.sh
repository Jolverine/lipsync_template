#!/bin/tcsh
# input --> missing_phones_list
set count = 1
set len = `cat $1|wc -l`
while ($count <= $len)
set word = `cat $1|head -$count|tail -1`
cat hmm_GMV/proto_3s_2m >> hmm_GMV/hmmdefs
sed -i s/proto_3s_2m/$word/ hmm_GMV/hmmdefs
@ count++
end
