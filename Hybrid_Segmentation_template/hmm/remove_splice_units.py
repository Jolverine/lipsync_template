import sys
import numpy 

labs = sys.argv[1]
lablist = open(labs,'r')
allfiles = lablist.readlines()
for i in range(len(allfiles)):
    labfile = open('syllab/'+allfiles[i][:-1],'r')
    lablines = labfile.readlines()
    lablines = lablines[1:]
    arr = []
    for j in range(len(lablines)):
        arr.extend([str.split(lablines[j])])
    #print arr
    if float(arr[0][0]) < 0.020:
        print("sed -i '/"+allfiles[i][:-5]+"-001"+"/d' Transcription_splice.txt")
        print("sed -i '/"+allfiles[i][:-5]+"-001"+"/d' map_table")
        print("sed -i '/"+allfiles[i][:-5]+"-001"+"/d' flist_splice")
    for k in range(1,len(arr)):
        if float(arr[k][0])-float(arr[k-1][0]) < 0.02:
            if k+1<10:
                print("sed -i '/"+allfiles[i][:-5]+"-00"+ str(k+1)+"/d' Transcription_splice.txt")
                print("sed -i '/"+allfiles[i][:-5]+"-00"+ str(k+1)+"/d' map_table")
                print("sed -i '/"+allfiles[i][:-5]+"-00"+ str(k+1)+"/d' flist_splice")
            elif k+1 < 100:
                print("sed -i '/"+allfiles[i][:-5]+"-0"+ str(k+1)+"/d' Transcription_splice.txt")
                print("sed -i '/"+allfiles[i][:-5]+"-0"+ str(k+1)+"/d' map_table")
                print("sed -i '/"+allfiles[i][:-5]+"-0"+ str(k+1)+"/d' flist_splice")
            elif k+1 < 1000:
                print("sed -i '/"+allfiles[i][:-5]+"-"+ str(k+1)+"/d' Transcription_splice.txt")
                print("sed -i '/"+allfiles[i][:-5]+"-"+ str(k+1)+"/d' map_table")
                print("sed -i '/"+allfiles[i][:-5]+"-"+ str(k+1)+"/d' flist_splice")
  
