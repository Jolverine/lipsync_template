#!/bin/tcsh -f
if( $# != 3 ) then
  echo " Argument ---> list input_folder output_folder"
endif

set num = `cat $1 | wc -l`
set count = 1
while ( $count <= $num )
    set fn = `head -$count $1 | tail -1`
    set wavfn = `echo $fn | cut -d "." -f1`
    echo "Syllable boundary correction for" $fn
    #set wsf = `grep $wavfn syllConfig | cut -d' ' -f2`
    tcsh front-end-dsp/Segmentation/scripts/group_delay_segmentation.sh $WAV_48/$wavfn.wav 6 200 100 0.5 0.2
    wait 
    tcsh front-end-dsp/Segmentation/scripts/group_delay_segmentation_flux.sh $WAV_48/$wavfn.wav 2 100 100 0.3
    wait
    cat results/$wavfn.boundaryEgy | cut -d " " -f5- > $wavfn-boundaryEgy 
    cat results/$wavfn.boundaryFlux | cut -d " " -f5- > $wavfn-boundaryFlux 
    echo "Boundary correction"
    #cat boundaryEgy
    #cat boundaryFlux
    ./syllable_boundary_correction $2/$fn $wavfn-boundaryEgy $wavfn-boundaryFlux > $3/$fn
    wait
    echo "Boundary correction completed for" $fn
    # cd results/
    # mv $wavfn.ste.txt STE/$wavfn-ste.txt
    # mv $wavfn.flux.txt SF/$wavfn-flux.txt
    # mv $wavfn.boundaryEgy.lab STE/$wavfn.lab
    # mv $wavfn.boundaryFlux.lab SF/$wavfn.lab
    # mv $wavfn.gd_spectrumEgy.txt STE/$wavfn-gdEgy.txt
    # mv $wavfn.magnitudeSpectrumFlux.txt SF/$wavfn-magFlux.txt
    # cd ../
    mv $wavfn-boundaryEgy results/STE/$wavfn-boundaryEgy
    mv $wavfn-boundaryFlux results/SF/$wavfn-boundaryFlux
    rm results/$wavfn*
    @ count++
end
