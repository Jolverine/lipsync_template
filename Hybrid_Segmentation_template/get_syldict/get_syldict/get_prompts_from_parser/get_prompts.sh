#!/bin/bash
less txt.done.data | parallel -j 10 "echo {1} > ./dump/data/{#}.txt"
ls dump/data/* |  parallel -k -j 10 "festival -b festvox/build_clunits.scm '(build_prompts \"{1}\")'"