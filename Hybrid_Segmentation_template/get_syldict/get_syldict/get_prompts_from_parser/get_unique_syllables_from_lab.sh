ls prompt-lab/* | parallel -k "tail -n +2 {1}" | awk '{ print $3 }' > unique_syllables
sort -u unique_syllables > sorted_unique_syllables
awk '{print $1}' ../syldict | sort -u > syllables_from_syldict
join -v 1 -v 2 <( sort sorted_unique_syllables ) <( sort syllables_from_syldict ) > missing_from_syldict
