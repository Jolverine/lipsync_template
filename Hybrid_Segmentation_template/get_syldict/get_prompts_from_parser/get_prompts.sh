#!/bin/bash
export FESTDIR="/common/software/festival_2.5/festival"
less txt.done.data | parallel -j 25 "echo {1} > ./dump/data/{#}.txt"
ls dump/data/* |  parallel -j 5 "/common/software/festival_2.5/festival/bin/festival -b festvox/build_clunits.scm '(build_prompts \"{1}\")'"
ls dump/data/*.txt | parallel -k -j 2 "awk '{print \$2}' {1}" > original_list
ls prompt-lab/*.lab | xargs -n 1 basename | sed -e 's/\.lab$//' > generated_list
grep -Fxv -f generated_list original_list > missing_lab_list
cat missing_lab_list | grep -o '....$' | sed 's/^0*//' | sed 's/$/.txt/' > to_be_done
cat to_be_done | parallel  -j 1 "/common/software/festival_2.5/festival/bin/festival -b festvox/build_clunits.scm '(build_prompts \"dump/data/{1}\")'"
./dumpfeats -feats feats1 -relation Word -output num_syl ./prompt-utt/*.utt
