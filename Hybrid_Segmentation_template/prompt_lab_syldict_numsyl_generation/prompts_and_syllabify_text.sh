#!/bin/bash
##############################################################################
#										#	
#	created by Mari Ganesh Kumar M					#
#	adapted by Mano Ranjith Kumar M					#	
#										#
#	date : Jul 26 2020							#
#										#
#	email : cs19s032@cse.iitm.ac.in					#
#										#	
##############################################################################

stage=-1
textfile=$1
syldict=$2
num_syl=$3
nj=$4
working_dir=$5

if [ -d ${working_dir}/dump ]; then
    rm -rf ${working_dir}/dump
    rm only_text.txt
    rm uniq_syl_list
    
fi
mkdir ${working_dir}/dump
awk '{$1=""; print $0}' $textfile > ${working_dir}/only_text.txt
python ${working_dir}/get_unique_words.py -f ${working_dir}/only_text.txt -o ${working_dir}/dump/words.txt
unique_words_file="dump/words.txt"

echo "switching working dir to local/phonify_text"
cd ${working_dir}
tmpdir=${working_dir}/dump/tmp
if [ $stage -le 0 ]; then
if [ -d "$tmpdir" ]; then
  rm -rf $tmpdir
fi

if [ -f "${num_syl}" ]; then
 rm ${num_syl}
fi

if [ -f "${syldict}" ]; then
 rm ${syldict}
fi

mkdir $tmpdir

echo ${working_dir}
echo ${unique_words_file}

cat ${working_dir}/${unique_words_file} | parallel -k -j $nj "valgrind phonify_text/unified-parser {1} $tmpdir/{1}.txt 1 1 0 0"
cat ${working_dir}/${unique_words_file} | parallel -k -j $nj "bash remove_set_wordstruct.sh $tmpdir/{1}.txt"


var1="$(ls $tmpdir/*.txt | wc -l)"
var4="$(less ${working_dir}/${unique_words_file} | wc -l)"

if [ $var1 -ne $var4 ]; then
echo " Unified-parser has not parsed for all words. Check words.txt"
exit 1
fi

fi
if [ $stage -le 1 ]; then
while IFS= read -r word; do
	output=`cat $tmpdir/${word}.txt| awk '{print NF}'`
	neword=`cat $tmpdir/${word}.txt`
	echo $neword >> ${working_dir}/uniq_syl_list
	echo $word $output >> ${num_syl}
done < ${working_dir}/$unique_words_file
sed -i "s/ /\n/g" ${working_dir}/uniq_syl_list
sort -u -o ${working_dir}/uniq_syl_list ${working_dir}/uniq_syl_list
sylsdir=${working_dir}/dump/syls

if [ -d "$sylsdir" ]; then
  rm -rf $sylsdir
fi 
mkdir $sylsdir
cat ${working_dir}/uniq_syl_list | parallel -k -j $nj "valgrind phonify_text/unified-parser {1} $sylsdir/{1}.txt 1 0 0 0"
cat ${working_dir}/uniq_syl_list | parallel -k -j $nj "bash remove_set_wordstruct.sh $sylsdir/{1}.txt"
while IFS= read -r word; do
	output=`cat $sylsdir/${word}.txt`
	echo $word $output >> ${syldict}
done < ${working_dir}/uniq_syl_list
echo "SIL SIL" >> ${syldict}
echo "sp sp" >> ${syldict}
fi
rm ${working_dir}/uniq_syl_list
cd ${working_dir}
python3 ${working_dir}/create_prompt_lab_num_syl_syl_dict.py ${textfile} 
echo "switching working dir back to ${working_dir}"

