#!/usr/bin/env python
#  Created by Mano Ranjith Kumar on 4/07/22
#  cs19s032@smail.iitm.ac.in
#  Input : Transcription - Punctuation free in espnet format
#  Output : Prompt-lab, Syllable Dictionary, And num_syl

import sys
import os
import shutil

def main():
	transcription = sys.argv[1]
	f = open(transcription, "r")
	lines = f.read().splitlines()

	f.close()
	syldict=[]
	try:
		os.makedirs('prompt-lab')
	except OSError as e:
		shutil.rmtree('prompt-lab')
		os.makedirs('prompt-lab')
	for line in lines:
		random_number=0.1100
		words=line.strip().split()
		with open("prompt-lab/"+words[0]+".lab","w") as fp:
			fp.writelines("#\n")
			#fp.writelines(str("%.4f"%random_number)+" "+str(100)+" "+"SIL"+"\n")
			#random_number+=0.1100
		for i in range(1,len(words)):
			if(words[i]=="*"):
				with open("prompt-lab/"+words[0]+".lab","a+") as fp:
					fp.writelines(str("%.4f"%random_number)+" "+str(100)+" "+"sp"+"\n")
					random_number+=0.1100
			else:
				try:
					file = open("dump/tmp/"+words[i]+".txt", 'r')
				except IOError:
					print('There was an error opening '+words[i]+' the file!')
					sys.exit()
				syllables=file.read().strip().split(" ")
				for syllable in syllables:
					with open("prompt-lab/"+words[0]+".lab","a+") as fp:
						fp.writelines(str("%.4f"%random_number)+" "+str(100)+" "+syllable+"\n")
						random_number+=0.1100
				file.close()
		#with open("prompt-lab/"+words[0]+".lab","a+") as fp:
			#fp.writelines(str("%.4f"%random_number)+" "+str(100)+" "+"SIL"+"\n")
			#random_number+=0.1100
if __name__ == "__main__":
    main()
