cat wav_list | sed 's/\.wav//g' > list1
cat list | sed 's/\.lab//g' > list2
comm -23 list2 list1 > remove_extra
cat remove_extra | sed 's/$/\.wav/g' > remove_extrawav
cat remove_extra | sed 's/$/\.lab/g' > remove_extralab
comm -23 list remove_extralab > lablist
mv lablist list
sed 's/\.lab/\.wav/g' list > wav_list
rm -f remove_extra* list1 list2
