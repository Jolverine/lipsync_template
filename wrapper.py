#!/usr/bin/python3

##############################################################################
#											
#	created by mano ranjith kumar m						
#										
#	date : Jul 26 2020							
#										
#	email : cs19s032@cse.iitm.ac.in					
#										
#	Description: A wrapper script to isochronously lip-sync		 
#		     the video with audio.					
#	Requirements: 								
#		(1) ESPnet/WaveGlow, along with model path for the same	
#		(2) MoviePy and FFMPEG for lipsyncing				
#		(3) SRT file -- From ASR system				
#		(4) Transcription file -- From MT system 			
#										
#	Arguments:								
#		(1) Working Directory (Should have kaldi_template in it)	
#		(2) Language of the synthesis					
#		(3) Gender of the required synthesis				
#										
#	Requirements: The script expects, transcription (Only text) present	
#	in the working directory. It synthesizes the transcription using	
#	phrase breaks and aligns using kaldi HMM-GMM ASR. It requires	
#	GPU for synthesizing waveforms					
#										
#	Acknowledgement : Credits to Jom Kuriakose, whose argument parser	
#		i've used in my code 							
#										
#	Sample command : python3 wrapper.py 					
#			-s ./V4_template/input_data/source.mp4 		
# 			-t Hindi -g male -srt V4_template/input_data/source.srt
#			-tts /tts1/tts1/anusha/TTS_models/Hindi_male		
#			 -m transcription					
#			-is no							
#										
#												
##############################################################################

# Import packages
import os, os.path
from os.path import exists
from os import listdir
from os.path import isfile, join
import subprocess
import sys
import argparse
from moviepy.editor import *
import moviepy.audio.fx.all as afx
from pathlib import Path
import pysrt
import shutil
from joblib import Parallel, delayed
import multiprocessing
import json
import timeit


######### User defined imports ###########

import V4_template.scripts.splitSrc_dict as splitSrc_class
import V4_template.scripts.calc_energy_dict as calcEnergy_class
import V4_template.scripts.splitAlignments_dict as splitAlignment_class
import V4_template.scripts.corr_with_lab_dict as corr_with_lab_class
import V4_template.scripts.corr_with_ene_dict as corr_with_ene_class
import V4_template.scripts.align_seg_dict as align_seg_class
import V4_template.scripts.put_commas_dict as put_commas_class
import V4_template.scripts.concat_syl_dict as concat_syl_class



# Print comment header
def code_head_print():
	code_head_str='A wrapper script to isochronously lip-sync the video\n\n'
	code_head_str="Requirements : Refer to the header on wrapper.pys"
	code_head_str+='Author: Mano Ranjith Kumar '
	code_head_str+='email: manoranjith@smail.iitm.ac.in'
	code_head_str+='Date: 22/04/2022\n\n'
	return code_head_str

# Print example command
def ex_cmd_print():
	ex_cmd_str='\nExample: python3 wrapper.py -s ./V4_template/input_data/source.mp4 -t Tamil -g female -srt V4_template/input_data/source.srt -tts /tts1/anusha/TTS_models/Tamil_female -m transcription -is no --conf config.json\n'
	return ex_cmd_str

# Parse arguments and print details
def parse_args():
	parser = argparse.ArgumentParser(description=code_head_print()+ex_cmd_print(),formatter_class=argparse.RawTextHelpFormatter)
	parser.add_argument('-s','--source_video', help='Source Video Lecture File', required=True)
	parser.add_argument('-t','--target_language', help='Target language', required=True)
	parser.add_argument('-g','--gender', help='Gender', required=True)
	parser.add_argument('-srt','--source_srt', help='Source SRT file', required=True)
	parser.add_argument('-m','--translated_text', help='Machine Translated text', required=True)
	parser.add_argument('-tts','--tts_model_for_synthesizing', help='Path to a folder which contains TTS models and Waveglow models',required=True)
	parser.add_argument('-is','--intro_segment_status', help='Does this video have a intro videos segment (1-yes/0-no)',required=True)
	parser.add_argument('-first_srt','--skip_flag_val', help='Does this video\'s SRT have intro message (1-yes/0-no)',required=True)
	parser.add_argument('-conf','--config_file', help='Point to the config json which contains the path',required=True)
	parser.add_argument('-userconf','--usr_config_file', help='Point to the user config json which containsthe values of user controlled variables',required=True)
	args = parser.parse_args()
	return args
	
def check_args():
	"""
	This function checks if the arguments are proper
	"""
	
	args=parse_args()
	allowed_languages=['Tamil','Hindi','Telugu','Malayalam','Bengali','Marathi']
	if(not(any(args.target_language in x for x in allowed_languages))):
		sys.exit("Lanugage not identified. Please choose one of the following languages: Hindi, Tamil, Telugu, Malayalam, Bengali, Marathi")
	allowed_gender=['male','female']
	if(not(any(args.gender in x for x in allowed_gender))):
		sys.exit("Gender is not identified. Please choose from male or female (case sensitive)")
	video_file = Path(args.source_video)
	if not video_file.is_file():
		sys.exit("Cannot find the source video file in this path given : " + str(args.source_video))
	srt_file = Path(args.source_srt)
	if not srt_file.is_file():
		sys.exit("Cannot find the SRT file for timestamps in this path given : " + str(args.source_srt))
	translated_file= Path(args.translated_text)
	if not translated_file.is_file():
		sys.exit("Cannot find the translated transcription file in this path given : " + str(args.translated_text))
	num_lines_of_translated_file = sum(1 for line in open(args.translated_text))
	timestamps = pysrt.open(args.source_srt)
	len_sub = len(timestamps)
	if(num_lines_of_translated_file != (len_sub-int(args.skip_flag_val))):
		sys.exit("The number of lines in the translated file and the number of srt segments in the subtitle file is different, they are " + str(num_lines_of_translated_file) +", "+str(len_sub)+" respectively")
	no_of_files_for_synth=len([name for name in os.listdir(args.tts_model_for_synthesizing) if os.path.isfile(os.path.join(args.tts_model_for_synthesizing, name))])
	if(no_of_files_for_synth < 7):
		sys.exit("The folder that contains espnet model and waveglow model is not present, the total number of files should be 8, whereas it has "+str(no_of_files_for_synth))
	if(int(args.intro_segment_status) != 1 and int(args.intro_segment_status) !=  0):
		sys.exit("The intro segment flag is not set")
	if(int(args.skip_flag_val) != 1 and int(args.skip_flag_val) != 0):
		sys.exit("The skip segment flag is not set")	
		
	config_json_file = Path(args.config_file)
	if not config_json_file.is_file():
		sys.exit("Configuration file was not found")
	config_usr_json_file = Path(args.usr_config_file)
	if not config_usr_json_file.is_file():
		sys.exit("User configuration file was not found")	
		
	print("Arguments parsed. All arguments are fine")
	return len_sub

def update_json(working_dir,data):
	"""
	This function updates the config.json, with current working directory
	"""
	for i in data:
		if (i != "silence_threshold" and i != "number_of_jobs"):
			data[i]=working_dir+str(data[i])
	return data
	
def clean_path(path):
	"""
	This function clears all the files in particular directory.
	"""
	folder = path
	for filename in os.listdir(folder):
		file_path = os.path.join(folder, filename)
		try:
			if os.path.isfile(file_path) or os.path.islink(file_path):
				os.unlink(file_path)
			elif os.path.isdir(file_path):
				shutil.rmtree(file_path)
		except Exception as e:
			print('Failed to delete %s. Reason: %s' % (file_path, e))
   		
def kaldi_alignment(target_language, gender,tts_model_path,translated_text):
	"""
	This function does synthesizes the TTS audio and performs Kaldi alignment.
	"""
	print("Synthesizing waveforms and performing kaldi alignment (This could take a while as the waveform needs to be synthesized. Please make sure of GPU's availbility for synthesis)")
	#p = subprocess.run(['bash run_kaldi.sh %s %s %s' % (target_language, gender,tts_model_path)],shell=True, capture_output=True, text=True)
	p = subprocess.Popen(['bash run_kaldi.sh %s %s %s %s' % (target_language, gender,tts_model_path,translated_text)],shell=True, text=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE,stdin=subprocess.PIPE)
	p.stdin.write("Please verify phrasified_text in the working directory, and press enter to continue")
	out,err=p.communicate()
	if(p.returncode != 0):
		text_file = open("lipsync.log", "w")
		n = text_file.write(err)
		my_file = open("my.log", "w")
		n = text_file.write(out)
		text_file.close()
		sys.exit("Problem with TTS synthesis/kaldi alignments. Please read lipsync.log for more details (Most proabable cause can be CUDA out of memory or some utterance would not have been synthesized properly (Refer Faulty files)")
	else:
		print("Kaldi alignments done sucessfully")

def hybrid_segment():
	"""
	This function does hybrid segmentation of the synthesized audio
	"""
	print("Performing Hybrid Segmentation")
	p = subprocess.run(['bash run_hybrid.sh'],shell=True, capture_output=True, text=True)
	if(p.returncode != 0):
		text_file = open("lipsync.log", "w")
		n = text_file.write(p.stdout)
		text_file.close()
		sys.exit("Problem with hybrid segmentation alignments. Please read lipsync.log for more details:"+ p.stderr)
	else:
		print("Obtained alignments sucessfully")

def compute_ste(working_dir,line):
	"""
	This function computers short term energy given a wav file
	"""
	file_name =  os.path.basename(os.path.splitext(line)[0])
	ste_file = str(working_dir+"/V4_template/temp_data/ste_boundaries/")+file_name+".ste"
	wav_file = line.strip('\n')
	print("Computing short-term-energies for " + str(file_name))
	calcEnergy_obj = calcEnergy_class.calc_energy(wav_file,ste_file)
	calcEnergy_obj.main()

def silence_detection(working_dir,audio_file):
	"""
	This function does silence detection for the source audio and saves it in directory.
	"""
	boundary_file_path =  str(working_dir+"/V4_template/temp_data/src_boundaries/")
	file_name = os.path.basename(os.path.splitext(audio_file)[0])
	cleaned_wav_file = os.path.splitext(audio_file)[0]+"_clean_wav.wav"
	p = subprocess.run(['ffmpeg -y -i %s -f wav -flags +bitexact -acodec pcm_s16le -ar 44100 -ac 1 %s' % (audio_file, cleaned_wav_file)], shell=True, capture_output=True, text=True)
	if(p.returncode != 0):
		text_file = open("lipsync.log", "w")
		n = text_file.write(p.stderr)
		text_file.close()
		sys.exit("Problem with FFMPEG audio extraction")
	print(" Extracting long silence for source audio: "+file_name)	
	p = subprocess.run(['bin/LongSilence bin/config/fe-words.base-src %s %s %s 80 40'% (cleaned_wav_file,str(boundary_file_path+file_name+".temp"),str(boundary_file_path+file_name+"_boundary_file.txt"))], shell=True, capture_output=True, text=True)
	if(p.returncode != 0):
		text_file = open("lipsync.log", "w")
		n = text_file.write(p.stderr)
		text_file.close()
		sys.exit("Problem with bin/LongSilence")
	
def doctmAlignments(ctm,target_dir):
	"""
	This function splits CTM boundaries into indivdual files
	"""
	print("Spliting ctm alignments...")
	ctmAlignments_obj = splitAlignment_class.splitAlignments(ctm,target_dir)
	target_boundary_list = ctmAlignments_obj.main()
	return set(target_boundary_list)

def corr_with_lab(ctm_file, lab_file,target_dir):
	"""
	This function corrects the boundaries with syllable segmentation.
	"""
	corr_with_lab_obj = corr_with_lab_class.corr_with_lab(ctm_file,lab_file,target_dir)
	corr_with_lab_obj.main()

def corr_with_ene(ctm_file, ste_file, target_dir):
	"""
	This function corrects the boundaries with energy.
	"""
	corr_with_ene_obj = corr_with_ene_class.corr_with_ene(ctm_file,ste_file,target_dir)
	corr_with_ene_obj.main()

def align_segments(a1,a2,a3,a4,a7,final_videos_dir):
	"""
	This function aligns each SRT segment with target audio, with help of boundary files provided.
	"""
	#print("Aligning segment")
	#print(a1,a2,a3,a4,output_align_list,output_dir_aligned_dir,a7,morpheme_tags)
	#print("\n")
	align_seg_obj = align_seg_class.align_seg(a1,a2,a3,a4,a7,final_videos_dir)
	align_seg_obj.main()
	
def lip_sync(working_dir,len_sub,srt,video,skip_flag,target_language,config,nj):
	"""
	This the main function which does lip-sycning
	"""	
	print("Performing a sanitary check on the files needed before proceeding to lip-syncing")
	
	if(exists(config["ctm_location"])):
		print("CTM file exsists, Proceeding...")
	else:
		sys.exit("CTM file is missing in"+str(working_dir+"/V4_template/target_ctm")+"exitting...")
	no_of_files_as_lab=len([name for name in os.listdir(config["lab_files"]) if os.path.isfile(os.path.join(config["lab_files"], name))])
	
	put_commas_obj = put_commas_class.put_commas(working_dir+"/transcription_with_commas.txt", config["ctm_location"])
	put_commas_obj.main()
	
	if(no_of_files_as_lab != len_sub-skip_flag):
		sys.exit("Lab files are missing, exitting...")
	else:
		print("All lab files present, proceeding...")
	no_of_audio_files=len([name for name in os.listdir(config["tts_files"]) if os.path.isfile(os.path.join(config["tts_files"], name))])
	if(no_of_audio_files != len_sub-skip_flag):
		sys.exit(" Audio files are missing, exitting...")
	else:
		print("All audio files present, proceeding...")
		
	########## Intitalizing ############
	print("Performing Lip-syncing")
	
	source_srt=srt
	source_video=video
	target_lang=target_language
	source_video_nosound = os.path.splitext(source_video)[0]+'_nosound.mp4'
	print("Extracting no_sound version of the video")
	if os.path.exists(source_video_nosound):
    		os.remove(source_video_nosound)
	p = subprocess.run(['ffmpeg -i %s -c copy -an %s'% (source_video, source_video_nosound)],shell=True, capture_output=True, text=True, check=True)
	if(p.returncode != 0):
		text_file = open("lipsync.log", "w")
		n = text_file.write(p.stdout)
		text_file.close()
		sys.exit("Problem with ffmpeg. Please read lipsync.log for more details:"+ p.stderr)
			
	print("Splitting the video segments")
	
	tts_audio_list = str(working_dir+"/V4_template/input_data/tts_aud_list")
	video_seg_list = str(working_dir+"/V4_template/temp_list/src_video_segs_list.txt")
	full_video_seg_list  = str(working_dir+"/V4_template/temp_list/srt_full_video_list.txt")
	out_loc = str(working_dir+"/V4_template/temp_data")
	splitSrc_obj = splitSrc_class.splitSrc(source_video, source_video_nosound,srt,tts_audio_list,video_seg_list,full_video_seg_list,out_loc,skip_flag)
	video_dict = splitSrc_obj.main()
	print("Video segments are split succesfully from source video")
	
	
	#######################################################################
	no_audio_video_dict={}
	print("Removing audio from the video segments")
	for key in video_dict:
		#print(key, video_dict[key])
		if not "intersrt" in key:
			video_clip = video_dict[key]
			video_clip_no_audio = video_clip.without_audio()
			basename = os.path.splitext(key)
			new_key = basename[0]+"_nosound"+basename[1]
			no_audio_video_dict[new_key] = video_clip_no_audio
	
	
	##########################################################################
	# Computing short-term-energy for synthesized audios.
	
	print("Computing short-term-energies")
		
	file1 = open(tts_audio_list, 'r')
	Lines = file1.readlines()
	Parallel(n_jobs=nj,backend="multiprocessing")(delayed(compute_ste)(working_dir,line) for line in Lines)
	
	###########################################################
	# Computing silence detection on source audio
	
	
	print("Performing silence detection on the source audio")
	source_audio_sec_loc=config["src_audio_segs"]+"/"
	source_audio_list = []
	for key in video_dict:
		if not "intersrt" in key:
			video_clip=video_dict[key]
			file_name =  os.path.basename(os.path.splitext(key)[0])
			video_clip = video_clip
			audio_clip=video_clip.audio
			audio_clip= audio_clip.set_fps(44100)
			audio_clip.write_audiofile(source_audio_sec_loc+file_name+".wav",codec='pcm_s16le')
			source_audio_list.append(str(source_audio_sec_loc+file_name+".wav"))
	
	Parallel(n_jobs=nj,backend="threading")(delayed(silence_detection)(working_dir,line) for line in source_audio_list)
	
	
	##################################################################
	# Preparing target boundaries from ctm
	
	print("Perparing target boudaries from CTM")
	ctm = config["ctm_location"]
	split_boundaries_directory = config["target_boundaries"]
	target_boundary_list = doctmAlignments(ctm,split_boundaries_directory)
	
	## Correction with lab files
	lab_file_list = [f for f in listdir(config["gd_boundaries"]) if isfile(join(config["gd_boundaries"], f))]
	
	for i in range(len(lab_file_list)):
		lab_file_list[i]=config["gd_boundaries"]+"/"+str(lab_file_list[i])
		
	gd_corrected_list=[]
	
	for line in target_boundary_list:
		file_name =  os.path.basename(os.path.splitext(line)[0])
		gd_corrected_list.append(config["gd_corrected_boundaries"]+"/"+file_name+".gtxt")
	
	new_target_boundary_list = list(target_boundary_list)
	new_target_boundary_list.sort()
	lab_file_list.sort()
	Parallel(n_jobs=nj,backend="multiprocessing")(delayed(corr_with_lab)(ctm_file,lab_file,config["gd_corrected_boundaries"]) for ctm_file,lab_file in zip(new_target_boundary_list,lab_file_list))
	
	## Correction with ste files
	
	ste_corrected_list=[]
	ste_list=[]
	for line in target_boundary_list:
		file_name =  os.path.basename(os.path.splitext(line)[0])
		ste_corrected_list.append(config["ste_corrected_boundaries"]+"/"+file_name+".stxt")
		
	ste_files = [f for f in listdir(config["ste_boundaries"]+"/") if isfile(join(config["ste_boundaries"]+"/", f))]
	
	for i in range(len(ste_files)):
		ste_files[i]=config["ste_boundaries"]+"/"+str(ste_files[i])
	count=0
	gd_corrected_list.sort()
	ste_files.sort()
	Parallel(n_jobs=nj,backend="threading")(delayed(corr_with_ene)(ctm_file,ste_file,config["ste_corrected_boundaries"]) for ctm_file, ste_file in zip(gd_corrected_list,ste_files))	
	
	
	############# Aligning segments
	
	## Inputs to align_segments
	#1# 
	all_source_boundary_list = [f for f in listdir(config["src_boundaries"]+"/") if isfile(join(config["src_boundaries"]+"/", f))]
	source_boundary_list = []
	for i in range(len(all_source_boundary_list)):
		if ("_boundary_file" in all_source_boundary_list[i]):
			source_boundary_list.append(all_source_boundary_list[i])
	for i in range(len(source_boundary_list)):
		source_boundary_list[i] = config["src_boundaries"]+"/" + source_boundary_list[i]
	source_boundary_list.sort()
	#print(len(source_boundary_list))
		
	#2#
	
	ste_corrected_boundary_list = [f for f in listdir(config["ste_corrected_boundaries"]+"/") if isfile(join(str(config["ste_corrected_boundaries"]+"/"), f))]
	
	for i in range(len(ste_corrected_boundary_list)):
		ste_corrected_boundary_list[i] = config["ste_corrected_boundaries"]+"/"+ste_corrected_boundary_list[i]
	ste_corrected_boundary_list.sort()
	#print(len(ste_corrected_boundary_list))
	#3#
	#print(len(no_audio_video_dict))
	#4#
	file1 = open(tts_audio_list, 'r')
	synthesized_audio_list = file1.readlines()
	for i in range(len(synthesized_audio_list)):
		synthesized_audio_list[i] = synthesized_audio_list[i].strip('\n')
	synthesized_audio_list.sort()
	#print(len(synthesized_audio_list))
	#5#
	output_align_list = str(working_dir+"/V4_template/temp_list/output_align_video_list.txt")
	#6#
	
	filename=[]
	for key in video_dict:
		basename = os.path.basename(os.path.splitext(key)[0]) 
		print(basename)
		if not "intersrt" in basename:
			filename.append(basename)
		else:
			try:
				video_dict[key].write_videofile(config["final_videos"]+"/"+basename+".mp4")
			except IndexError:
				video_dict[key] = video_dict[key].subclip(t_end=(video_dict[key].duration - 1.0/video_dict[key].fps))
				video_dict[key].write_videofile(config["final_videos"]+"/"+basename+".mp4")
	filename.sort()	 
	#print(len(filename))
	
	#8#
	final_videos_dir = str(working_dir+"/V4_template/")
	
	
	Parallel(n_jobs=1,backend="threading")(delayed(align_segments)(a1,a2,no_audio_video_dict[a3],a4,a7,final_videos_dir) for a1,a2,a3,a4,a7 in zip(source_boundary_list,ste_corrected_boundary_list,no_audio_video_dict,synthesized_audio_list,filename))	
	
	
def load_config_file(path):
	"""
	This function loads the config.json file to the script
	"""
	with open(path) as myfile:
    		data=myfile.read()
	obj = json.loads(data)
	return obj


def clear_directories(config):
	"""
	This is a helper function to clean certain directories mentioned in the config.json. Config.json also has certain values that need not to be cleaned.
	"""
	answer = input("The program would like your permission to clean the temporary directories, incase if you are re-running again (Y/N). Incase you choose no, make sure the directories are empty and backed up if needed\n")
	if answer.lower() in ["y","yes"]:
		for key in config:
			if(key != "morpheme_tags" and key != "phrase_tags" and key !="ctm_location" and key!="tts_files" and key!="lab_files" and key!="subtitle_file" and key!="silence_threshold" and key!="number_of_jobs" and key!= "silence_detection_file" and key!="num_syl"):
				clean_path(config[key])
	elif answer.lower() in ["n","no"]:
		print("Not deleting, proceeding")
	else:
		print("Not a valid response, not deleting, proceeding")
		
def add_srt_synthesize_final_video(videos_path,working_dir,subtitle_file,len_sub, intro_seg_val):
	"""
	This function adds the subtitles to the final videos and creates the final videos
	"""
	
	no_of_output_videos=len([name for name in os.listdir(videos_path) if os.path.isfile(os.path.join(videos_path, name))])
	if(no_of_output_videos < len_sub):
		print(len_sub)
		print(no_of_output_videos)
		sys.exit("One of the videos, is not synthesized")
	else:
		p = subprocess.run(['bash add_srt.sh %s %s %s %s' % (videos_path, working_dir,subtitle_file,str(intro_seg_val))],shell=True, capture_output=True, text=True, check=True)
		if(p.returncode != 0):
			text_file = open("lipsync.log", "w")
			n = text_file.write(p.stdout)
			text_file.close()
			sys.exit("Problem with subtitles")

def change_sil_threshold(threshold,path):
	"""
	This function changes the value of silence detection used in source audio
	"""
	
	with open(path, 'r') as file:
		data = file.readlines()
	has_been_set=0
	for i in range(len(data)):
		if("thresEnergy float " in data[i]):
			data[i] ="thresEnergy float "+str(threshold)+"\n"
			has_been_set=1
			break
	if(has_been_set==0):
		print("Energy line not found,adding")
		data.append("thresEnergy float "+str(threshold)+"\n")
	with open(path, 'w') as file:
    		file.writelines(data)
    		
    		
def main():
	len_sub = check_args()
	args=parse_args()
	source_video=args.source_video
	srt_file=args.source_srt
	target_language=args.target_language
	gender = args.gender
	intro_seg_sts = int(args.intro_segment_status)
	skip_flag_sts = int(args.skip_flag_val)
	translated_text = args.translated_text
	tts_model_path = args.tts_model_for_synthesizing
	my_data_file = args.config_file
	working_dir = os.getcwd()
	my_json_data = load_config_file(args.config_file)
	usr_json_data = load_config_file(args.usr_config_file)
	json_data = dict(my_json_data)
	json_data.update(usr_json_data)
	config = update_json(working_dir,json_data)
	clear_directories(config)
	nj=int(config["number_of_jobs"])
	
	starttime = timeit.default_timer()
	print("The start time is :",starttime)
	response=""
	while(True):
		response=input("Do you want to synthesize the audio (y/n) ?( Please respond no if the audio has already been synthesized and verified )")
		if(response.lower() =="yes" or response.lower() =="y" or response.lower() =="n" or response.lower() =="no"):
			break
		print("Please provide a valid response \n")
	if(response.lower() =="yes" or response.lower() =="y"):
		kaldi_alignment(target_language, gender,tts_model_path,translated_text)
		print("The time difference is :", timeit.default_timer() - starttime)
	starttime = timeit.default_timer()
	hybrid_segment()
	print("The time difference is :", timeit.default_timer() - starttime)
	starttime = timeit.default_timer()
	concat_syl_obj = concat_syl_class.concat_syl(config["num_syl"], working_dir+"/transcription.txt",config["lab_files"],config["gd_boundaries"])
	concat_syl_obj.main()
	change_sil_threshold(config["silence_threshold"],config["silence_detection_file"])
	lip_sync(working_dir,len_sub,srt_file,source_video,skip_flag_sts,target_language, config,nj)
	print("The time difference is :", timeit.default_timer() - starttime)
	starttime = timeit.default_timer()
	print("Embedding SRT and preparing the video")
	add_srt_synthesize_final_video(config["final_videos"],working_dir,config["subtitle_file"],len_sub, intro_seg_sts)
	print("The time difference is :", timeit.default_timer() - starttime)
	starttime = timeit.default_timer()
	
if __name__ == "__main__":
    main()
	
