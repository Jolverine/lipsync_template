##############################################################################
#										#										
#	created by : SMT Lab, Further adapted by mano ranjith kumar m	#
#		     for-lipsycning						#								
#										#
#	date : Jul 26 2020							#
#										#
#	email : cs19s032@cse.iitm.ac.in					#								
#										#
#	Description: This code performs hybrid syllable segementation 	#
#	and provides lab files							#
#										#
#	Arguments:								#
#		Does not take any arguments					#
#										#
#	Requirements: The script expects, transcription_without_phrasing	#
#	to be present in the working directory. The directory is audio	#
#	is expected to be present. This script is run after run_kaldi.sh	#
#										#		
##############################################################################

working_dir=$PWD
textfile=$PWD/transcription.txt
nj=$(wc -l $textfile | cut -d' ' -f1) # number of parallel jobs
if [ $nj -gt 20 ]
then
        nj=20
fi
if [ $nj -eq 0 ]
then
        nj=1
fi
export NJ=$nj

##	Resampling the audio files	##

if [ -d "$working_dir/audio_48khz" ]; then rm -r $working_dir/audio_48khz; fi
mkdir $working_dir/audio_48khz
mkdir $working_dir/mfc
for i in $working_dir/audio/*.wav; do v=$(basename $i) && sox $i -r 48000 $working_dir/audio_48khz/${v}; done

##	Prompt-labs and syldict creation	##

cp $working_dir/transcription.txt $working_dir/Hybrid_Segmentation_template/get_syldict/transcription.txt
cd $working_dir/Hybrid_Segmentation_template/get_syldict/
bash format_e2e_to_festival.sh transcription.txt
cp txt.done.data get_prompts_from_parser/
cd get_prompts_from_parser
rm dump/*.txt
rm num_syl
rm dump/data/*
rm prompt-lab/*.lab
rm prompt-utt/*.utt
bash get_prompts.sh
rm sorted_unique_syllables
bash get_unique_syllables_from_lab.sh
bash non_parallel-parser.sh sorted_unique_syllables syldict ${nj}
sed -i 's/\" \"/ /g' syldict
sed -i "s/(set\!\ wordstruct\ '(\ ((\ \"/beg-/g" syldict
sed -i "s/\"\ )\ 0)\ ))/_end/g" syldict
sed -i "s/\"\ )\ 0)\ ((\ \"/ /g" syldict
rm $working_dir/Hybrid_Segmentation_template/hmm/syldict
rm -rf $working_dir/Hybrid_Segmentation_template/hmm/prompt-lab
cp syldict $working_dir/Hybrid_Segmentation_template/hmm/
cp num_syl $working_dir
cp -r prompt-lab $working_dir/Hybrid_Segmentation_template/hmm/
cd $working_dir


##	MFCC extraction	##

rm $working_dir/map_table
rm $working_dir/audio_lst
rm -r $working_dir/mfc/*
ls $working_dir/audio_48khz/* > audio_lst
tcsh $working_dir/Hybrid_Segmentation_template/scripts/map_table.sh audio_lst ## Make sure that map_table.sh has $1 instead of map_table
cd $working_dir/Hybrid_Segmentation_template
bash scripts/extract_feature.sh ../map_table
cd ..
mv $working_dir/audio_48khz/*.mfc $PWD/mfc

cd $working_dir/Hybrid_Segmentation_template

##	Hybrid Segmentation (SMT Lab code slightly modified to make it run in a command)	##

export DSPLIB=$PWD/front-end-dsp/src
export NISTINC=$PWD/front-end-dsp/nist/include
export NISTLIB=$PWD/front-end-dsp/nist/lib
export WAV_48=$PWD/../audio_48khz
export MFC_48=$PWD/../mfc

sh src/create_directories.sh

echo "=========================================================================="
echo " 				Block 1 complete				"
echo "=========================================================================="

echo "HS start" >>timelog.txt
date >> timelog.txt
cd hmm/
cat syldict | cut -d " " -f2- > temp
cp syldict syldict_without_context
sed -e 's/beg\-//g' -i syldict_without_context
sed -e 's/\_end//g' -i syldict_without_context
sed -e 's/\s\+/\n/g' -i temp
sort -u temp > phonelist
cat syldict_without_context | cut -d " " -f2- > temp
sed -e 's/\s\+/\n/g' -i temp
sort -u temp > phonelist_without_context
cd ../
cp hmm/syldict_without_context dict_with_end
sed -e 's/$/ end/' -i dict_with_end
cp Phonelist_Description/Vowels hmm/vowels


cd hmm/

ls prompt-lab > list
ls $WAV_48 > wav_list
sh scripts/prepare_list.sh
cp list ../
rm dict_with_context
tcsh scripts/dict.sh phonelist_without_context || exit -1
tcsh scripts/dict_with_context.sh phonelist

echo "=========================================================================="
echo " 				Block 2 complete				"
echo "=========================================================================="

sort phonelist_without_context > phonelist_sorted
sort vowels > vowels_sorted
comm -23 phonelist_sorted vowels_sorted > consonants
sed -i '/sp/d' consonants

perl scripts/create_transcription.pl
./createMLF

HLEd -l '*' -d syldict_without_context -i phones.mlf mkphones0.led words.mlf
HLEd -l '*' -d syldict -i phones_with_context.mlf mkphones0.led words.mlf
sed -i 's/wav_8KHz/*/g' words.mlf

echo "=========================================================================="
echo " 		 Block 2(a) complete - output in words.mlf			"
echo "=========================================================================="

sed 's#^#'"$MFC_48/"'#g' wav_list > flist
sed -i 's/\.wav/\.mfc/g' flist
HCompV -C config_files/config_feature -f 0.01 -m -S flist -M hmm_GMV protos/proto_3s_2m
HCompV -C config_files/config_feature -f 0.01 -m -S flist -M hmm_GMV protos/proto_5s_2m
HCompV -C config_files/config_feature -f 0.01 -m -S flist -M hmm_GMV protos/proto_1s_2m
cat HMM_macro_header hmm_GMV/vFloors > hmm_GMV/macros
grep -Fxv -f globalPhoneList phonelist > missing_phones_list
grep -Fxv -f globalPhoneList phonelist_without_context >> missing_phones_list
sort -u missing_phones_list -o missing_phones_list
cp $working_dir/trained_models/one_model/models_hmm/reestimatedhmms/hmmdefs ./hmm_GMV/.
cp $working_dir/trained_models/one_model/models_hmm/macros ./hmm_GMV/macros
if [ -s missing_phones_list ]; then
        # The file is not-empty.
	tcsh scripts/add_missing.sh missing_phones_list
fi
echo "=========================================================================="
echo " 				Block 3 complete				"
echo "=========================================================================="

bash scripts/splitMLFrun.sh flist phones.mlf $NJ flistpart
ls flistpart/*.mlf > mlftemp
ls flistpart/*.lst >lsttemp
ls flistpart/*.lst | cut -d'/' -f2 | cut -d'.' -f1 >lstnum
paste -d ' ' mlftemp lsttemp lstnum > mlflstmap
rm mlftemp lsttemp lstnum

tcsh scripts/FS_HERest.sh 0 1 14 $NJ flistpart ### while using 10hr database, put a greater number say 8 or 16, otherwise might get error in reestimation

echo "=========================================================================="
echo " 			Block 4 (Flatstart HERest) complete 			"
echo "=========================================================================="


echo "Sentence Lvl Alignment with SIL"
HVite -l output_lab_with_SIL/ -C config_files/config -a -H hmm_GMV/hmm14/macros -H hmm_GMV/hmm14/hmmdefs -y lab -o SM -I words.mlf -S flist syldict_without_context phonelist_without_context


echo "=========================================================================="
echo " 				Block 5 complete				"
echo "=========================================================================="



perl scripts/normallab_with_SIL.pl


rm -rf part* sublists
split -da 4 -l $((`wc -l < list`/$NJ)) list part --additional-suffix=".sublist"
partcount=`ls part*|wc -l`;
echo "count part : $partcount"
ls part* > sublists


rm -f output_lab_temp/*
less sublists | parallel -v -j$NJ "tcsh scripts/remsil_initial.sh {}"

echo "=========================================================================="
echo " 				Block 6 complete				"
echo "=========================================================================="


rm Transcription.txt
perl scripts/create_transcription_SIL_removed.pl
mv words.mlf words_with_SIL.mlf
./createMLF
sed -i 's/wav_8KHz/*/g' words.mlf

echo "=========================================================================="
echo " 				Block 7 complete				"
echo "=========================================================================="


echo "Sentence Lvl Alignment"
HVite -l output_lab/ -C config_files/config -a -H hmm_GMV/hmm14/macros -H hmm_GMV/hmm14/hmmdefs -y lab -o SM -I words.mlf -S flist syldict_without_context phonelist_without_context
perl scripts/normallab.pl

echo "=========================================================================="
echo " 				Block 8 complete				"
echo "=========================================================================="


cd ../



rm -rf part* sublists
split -da 4 -l $((`wc -l < list`/$NJ)) list part --additional-suffix=".sublist"
partcount=`ls part*|wc -l`;
echo "count part : $partcount"
ls part* > sublists
echo "Adding Begin & End Phone to Syllable Lab Files.... I"
rm -f hmm_syllable_lab_with_begin_end_phone/*
less sublists | parallel -v -j$NJ "tcsh src/add_phone2syl.sh {}"

echo "=========================================================================="
echo " 				Block 9 complete				"
echo "=========================================================================="


rm -f intermediate_output_lab/*
echo "Syllable Boundary Correction.... I"
less sublists | parallel -v -j$NJ "tcsh src/gd_correction.sh {} hmm_syllable_lab_with_begin_end_phone intermediate_output_lab"
cp sublists part* hmm/

cp intermediate_output_lab/* ../V4_template/lab_files/

cp intermediate_output_lab/* hmm/syllab_with_sil/
cd hmm/
echo "Remove very small silences.... I"
rm -f syllab/*
less sublists | parallel -v -j$NJ "tcsh scripts/remsil.sh {}"

echo "=========================================================================="
echo " 				Block 10 complete				"
echo "=========================================================================="

####ok till here

echo "Splicing Waveforms & Extracting Features.... I"
rm -f Transcription_splice.txt
rm -f splice_wav/*
less sublists | parallel -v -j$NJ "perl scripts/splice.pl {}"



rm sublists part*
./createMLF_splice
ls splice_wav/ > wav_list_splice
sed -i 's/^/splice_wav\//' wav_list_splice 
rm map_table



find splice_wav -name "t*" | sort > a1
cp a1 a2
sed -i 's/\.wav/.mfc/g' a2
paste -d' ' a1 a2  > map_table
rm a1 a2

echo "=========================================================================="
echo " 				Block 11 complete				"
echo "=========================================================================="

cat map_table | cut -d " " -f2 > flist_splice
python remove_splice_units.py list > log_bad.sh

perl scripts/logbadfast.pl log_bad.sh flist_splice map_table Transcription_splice.txt 


##ok till here

./createMLF_splice
rm -rf part* sublists
split -da 4 -l $((`wc -l < map_table`/$NJ)) map_table part --additional-suffix=".sublist"
ls part* > sublists
less sublists | parallel -v -j$NJ "sh scripts/extract_feature_splice.sh {}"
rm sublists part*

HLEd -l '*' -d syldict -i phones_splice.mlf mkphones0.led words_splice.mlf
cp hmm_GMV/hmmdefs hmm0/hmmdefs
cp hmm_GMV/macros hmm0/macros
echo "HERest I start" >>../timelog.txt
date >> ../timelog.txt

echo "=========================================================================="
echo " 				Block 12 complete				"
echo "=========================================================================="

echo "Embedded Reestimation.... I"


HERest -C config_files/config_feature_2 -I phones_splice.mlf -t 250.0 150.0 6000.0 -S flist_splice -H hmm0/macros -H hmm0/hmmdefs -M hmm1 phonelist
HERest -C config_files/config_feature_2 -I phones_splice.mlf -t 250.0 150.0 6000.0 -S flist_splice -H hmm1/macros -H hmm1/hmmdefs -M hmm2 phonelist
HERest -C config_files/config_feature_2 -I phones_splice.mlf -t 250.0 150.0 6000.0 -S flist_splice -H hmm2/macros -H hmm2/hmmdefs -M hmm3 phonelist
HERest -C config_files/config_feature_2 -I phones_splice.mlf -t 250.0 150.0 6000.0 -S flist_splice -H hmm3/macros -H hmm3/hmmdefs -M hmm4 phonelist
HERest -C config_files/config_feature_2 -I phones_splice.mlf -t 250.0 150.0 6000.0 -S flist_splice -H hmm4/macros -H hmm4/hmmdefs -M hmm5 phonelist
HERest -C config_files/config_feature_2 -I phones_splice.mlf -t 250.0 150.0 6000.0 -S flist_splice -H hmm5/macros -H hmm5/hmmdefs -M hmm6 phonelist
HERest -C config_files/config_feature_2 -I phones_splice.mlf -t 250.0 150.0 6000.0 -S flist_splice -H hmm6/macros -H hmm6/hmmdefs -M hmm7 phonelist
echo "=========================================================================="
echo "		Block 13 (Embedded reestimation I - HERest) complete				"
echo "=========================================================================="

HVite -l output_lab_splice/ -C config_files/config_splice -a -H hmm7/macros -H hmm7/hmmdefs -y lab -o SM -I words.mlf -S flist syldict phonelist 

echo "=========================================================================="
echo " 	    Block 14 complete - output in output_lab_splice/ folder		"
echo "=========================================================================="

echo "HERest I end" >>../timelog.txt
date >> ../timelog.txt

perl scripts/normallab_splice.pl

cd ../
echo "Adding Begin & End Phone to Syllable Lab Files.... II"
rm -f hmm_syllable_er_syl_lab_with_begin_end_phone/*
less sublists | parallel -v -j$NJ "tcsh src/add_phone2syl_2.sh {}"
rm -f output_lab_syllable/*
echo "Syllable Boundary Correction.... II"
less sublists | parallel -v -j$NJ "tcsh src/gd_correction_2.sh {} hmm_syllable_er_syl_lab_with_begin_end_phone output_lab_syllable"

sed -e 's/sp/SIL/g' -i output_lab_syllable/*
cp -r output_lab_syllable/* $working_dir/V4_template/lab_files
