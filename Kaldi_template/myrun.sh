. ./cmd.sh
. ./path.sh

feats_nj=10
train_nj=10
decode_nj=10
courseID=108102113
mkdir -p dump1/$courseID
mkdir -p Exp
text_tmp=dump1/$courseID/text.tmp
segments_tmp=dump1/$courseID/segments.tmp
text=dump1/$courseID/text
words=dump1/$courseID/words
dict=dump1/$courseID/dict
segments=dump1/$courseID/segments
ctm=dump1/$courseID/ctm
srt=dump1/$courseID/srt

stage=1
#count=`less temp.lst |wc -l`
#let count=1
if [ $stage -le 0 ]; then

echo ============================================================================
echo "                     Data preparation                        "
echo ============================================================================

less temp.lst |parallel -k --colsep ' ' "dos2unix {1}/{2}/{3}.srt"
less temp.lst |parallel -k --colsep ' ' "less {1}/{2}/{3}.srt |grep '\-\->' | sed 's/,/:/g' | sed 's/-->/:/g' | sed 's/\ //g' | sed s/\ /:/ | awk -v id1={2} -v id2={3} -F\":\" '{printf \"%s %s %s %s %04d %0.3f %0.3f\n\",id1, id2, id1, id2, NR,\$3+(60*\$2)+(3600*\$1)+(\$4/1000),\$7+(60*\$6)+(3600*\$5)+(\$8/1000)}' |sed 's/ /_/' |sed 's/ /_/' |sed 's/ /_/' |sed 's/ /_/' |sed 's/_/ /' |sed 's/_/ /' |sed 's/ /_/' |awk '{print \$2\" \"\$1\" \"\$3\" \"\$4}'" >$segments_tmp
less temp.lst |parallel -k --colsep ' ' "grep -v '\-\->' {1}/{2}/{3}.srt |tr '\r' '\n' |sed 's/.*/\U&/' |awk -v id1={2} -v id2={3} -v i=1 '{if(\$1==i && NF==1){printf \"\n%s %s %04d \", id1, id2, i; i++}else if(\$1!=i&& NF>0){printf \"%s \", \$0}}END{printf \"%s\",\$0}' |sed  's/[^a-zA-Z0-9 ]/ /g' |sed 's/  / /g' |sed 's/ /_/' |sed 's/ /_/' |sed '1d' |awk 'NF>1{print \$0}'" > $text_tmp
join -1 1 -2 1 <(sort -k1 $text_tmp) <(sort -k1 $segments_tmp) |awk '{print $1" "$(NF-2)" "$(NF-1)" "$NF}' > $segments
join -1 1 -2 1 <(sort -k1 $text_tmp) <(sort -k1 $segments_tmp) |awk '{for(i=1;i<=NF-3; i++){printf "%s ", $i};printf "\n"}' > $text
cut -d' ' -f2- $text |tr ' ' '\n' |sort -u |sed '1d' > $words

perl /common/software/cmusphinx/logios/Tools/MakeDict/make_pronunciation.pl -tools /common/software/cmusphinx/logios/Tools -dictdir dump1/$courseID -words words -dict dict
cp dict dict-new
less digits_lex.txt $dict |sort |awk '{if($1==prev){printf "%s(%d)\t", $1,j; j++}else{printf "%s\t", $1; j=1}; for(i=2;i<=NF;i++){printf "%s ",$i}; printf "\n"; prev=$1}' |sed '1 i\SIL\tSIL' > dump1/$courseID/newDict
#less abb.txt dump1/$courseID/newDict |sort |awk '{if($1==prev){printf "%s(%d)\t", $1,j; j++}else{printf "%s\t", $1; j=1}; for(i=2;i<=NF;i++){printf "%s ",$i}; printf "\n"; prev=$1}' |sed '1 i\SIL\tSIL' > dump1/$courseID/newDict_after_abb

#Grep only capital letters
less temp.lst |parallel -k --colsep ' ' "grep -v '\-\->' {1}/{2}/{3}.srt | grep -o '[^ ]*[A-Z][^ ]*' "

rm -rf data/*
mkdir -p data/local/lm
mkdir -p data/local/dict
cp templateDir/* data/local/dict/

sed -i '1 i\SIL\tSIL' $dict
#cp dump1/$courseID/newDict_after_abb data/local/dict/lexicon.txt
cp dump1/$courseID/newDict data/local/dict/lexicon.txt
mkdir -p data/train
cp $text data/train/text

bash utils/prepare_lang.sh --sil-prob 0.0 --position-dependent-phones false --num-sil-states 3 data/local/dict "SIL" data/local/lang_tmp data/lang
bash train_lms_srilm1.sh data data/local/lm_tmp
compile-lm data/local/lm_tmp/4gram.me.gz -t=yes /dev/stdout |grep -v UNK | gzip -c > data/local/lm/lm_phone_bg.arpa.gz
gunzip -c data/local/lm/lm_phone_bg.arpa.gz | egrep -v '<s> <s>|</s> <s>|</s> </s>' | arpa2fst - | fstprint |utils/eps2disambig.pl |utils/s2eps.pl | fstcompile --isymbols=data/lang/words.txt --osymbols=data/lang/words.txt  --keep_isymbols=false --keep_osymbols=false | fstrmepsilon | fstarcsort --sort_type=ilabel > data/lang/G.fst
mkdir -p data/train
less temp.lst |parallel -k --colsep ' ' "awk -v loc={1} -v id1={2} -v id2={3} 'BEGIN{print id1\"_\"id2\" \"loc\"/\"id1\"/\"id2\".wav\"}'" > data/train/wav.scp
cut -d' ' -f1,2 $segments > data/train/utt2spk
perl utils/utt2spk_to_spk2utt.pl data/train/utt2spk > data/train/spk2utt
cp $segments data/train/segments

fi
if [ $stage -le 1 ]; then

echo ============================================================================
echo "                     Feature extraction                        "
echo ============================================================================

steps/make_mfcc.sh --cmd "$train_cmd" --nj $feats_nj data/train || exit 1;
steps/compute_cmvn_stats.sh data/train || exit 1;

fi

if [ $stage -le 2 ]; then

echo ============================================================================
echo "                     Training                         "
echo ============================================================================

steps/train_mono.sh --boost-silence 1.25 --nj "$train_nj" data/train data/lang exp/mono 
steps/align_si.sh --boost-silence 1.25 --nj "$train_nj" data/train data/lang exp/mono exp/mono_ali
steps/train_deltas.sh --boost-silence 1.25 2000 10000 data/train data/lang exp/mono_ali exp/tri1
steps/align_si.sh --nj "$train_nj"  data/train data/lang exp/tri1 exp/tri1_ali
steps/train_lda_mllt.sh --splice-opts "--left-context=3 --right-context=3" 2500 15000  data/train data/lang exp/tri1_ali exp/tri2
steps/align_si.sh  --nj "$train_nj" data/train data/lang exp/tri2 exp/tri2_ali
steps/train_sat.sh 4200 40000 data/train data/lang exp/tri2_ali exp/tri3
steps/align_fmllr.sh --nj 1 data/train data/lang exp/tri3 exp/tri3_ali
steps/get_train_ctm.sh data/train data/lang/ exp/tri3_ali

fi

if [ $stage -le 3 ]; then

echo ============================================================================
echo "                     Interpausal splicing                         "
echo ============================================================================


txt=dump1/$courseID/txt
segments=dump1/Segments

less temp.lst |parallel -k --colsep ' ' "./phnrec -i {1}/{2}/{3}.wav -c configDir/PHN_HU_SPDAT_LCRC_N1500/ -t str -o {1}/{2}/{3}.txt"
less temp.lst |parallel -k --colsep ' ' "less {1}/{2}/{3}.txt" > $txt

less temp.lst |parallel -j20 --colsep ' ' "awk '{print \$1/10000000\" \"\$2/10000000\" \"\$3}' {1}/{2}/{3}.txt |awk -v id1={2} -v id2={3} -v ind=1 -v j=0 '{if((\$3==\"pau\" || \$3==\"int\" || \$3==\"spk\") && ind==0 && (time >=10 || \$2-\$1 >10) ){j++; d1=\$2-\$1; printf \"%s_%s_%04d %s_%s %f %f\n\",id1,id2,j,id1,id2,t1,t2+d1/2 ;ind=1;time=0} else if(\$2-\$1>=1.0 && (\$3==\"pau\" || \$3==\"int\" || \$3==\"spk\") && ind==0 && time >=5 ){j++; d1=\$2-\$1; printf \"%s_%s_%04d %s_%s %f %f\n\",id1,id2,j,id1,id2,t1,t2+0.5 ;ind=1;time=0} else if(ind==0){t2=\$2;time+=\$2-\$1}; if(ind==1 && (\$3==\"pau\" || \$3==\"int\" || \$3==\"spk\") ) {t1=\$2-d1/2;t2=\$2;ind=0;time=0} }'" > dump1/Segments

#ind=1 initialises a segment. Until a segment is greater than 5 seconds, the segment is aggregated. If the segment duration is between (5-10)s and if the duration of sil/noise is greater than 1 sec, the segment is spliced. Or if the segment is greater than 10s and if a sil/noise is seen, the segment is spliced right away. Also, if the current sil is greater than 10 sec, the segment is spliced right away.

fi

if [ $stage -le 4 ]; then

echo ============================================================================
echo "                     Make graph                         "
echo ============================================================================
count=10
mkdir dump2
words=dump2/words
dict=dump2/dict
segments=dump1/Segments
mkdir -p data2
rm -rf data2/lang*
for i in `seq 1 $count`
	do
	rm -rf data1/*
	rm -rf dump2/*
	lecID=`less temp.lst |cut -d' ' -f3 |head -$i |tail -1`
	grep "_$lecID" data/train/text |cut -d' ' -f2- |tr '\n' ' ' |sed 's/  / /g' > data1/text
	grep "_$lecID" data/train/text |cut -d' ' -f2- |sed 's/ /\n/g' |sort |uniq |sed '1d' > dump2/words
	perl /common/software/cmusphinx/logios/Tools/MakeDict/make_pronunciation.pl -tools /common/software/cmusphinx/logios/Tools -dictdir dump2 -words words -dict dict
	mkdir -p data1/local/lm
	mkdir -p data1/local/dict
	cp templateDir/* data1/local/dict/
	less digits_lex.txt $dict |sort |awk '{if($1==prev){printf "%s(%d)\t", $1,j; j++}else{printf "%s\t", $1; j=1}; for(i=2;i<=NF;i++){printf "%s ",$i}; printf "\n"; prev=$1}' |sed '1 i\SIL\tSIL' > data1/local/dict/lexicon.txt
	bash utils/prepare_lang.sh --sil-prob 0.0 --position-dependent-phones false --num-sil-states 3 data1/local/dict "SIL" data1/local/lang_tmp data1/lang
	bash train_lms_srilm2.sh data1 data1/local/lm_tmp
	compile-lm data1/local/lm_tmp/4gram.me.gz -t=yes /dev/stdout |grep -v UNK | gzip -c > data1/local/lm/lm_phone_bg.arpa.gz
	gunzip -c data1/local/lm/lm_phone_bg.arpa.gz | egrep -v '<s> <s>|</s> <s>|</s> </s>' | arpa2fst - | fstprint |utils/eps2disambig.pl |utils/s2eps.pl | fstcompile --isymbols=data1/lang/words.txt --osymbols=data1/lang/words.txt  --keep_isymbols=false --keep_osymbols=false | fstrmepsilon | fstarcsort --sort_type=ilabel > data1/lang/G.fst

	cp -r data1/lang data2/lang$i
	rm -rf exp/*/graph$i
	#utils/mkgraph.sh data1/lang exp/mono exp/mono/graph$i
	#utils/mkgraph.sh data1/lang exp/tri1 exp/tri1/graph$i
	#utils/mkgraph.sh data1/lang exp/tri2 exp/tri2/graph$i
	#utils/mkgraph.sh data1/lang exp/tri3 exp/tri3/graph$i
#	rm -rf exp/*/decode$i
done

for i in `seq 1 $count`
	do
	rm -rf data/test$i/*
	mkdir -p data/test$i
	lecID=`less temp.lst |cut -d' ' -f3 |head -$i |tail -1`
	grep "_$lecID" $segments > data/test$i/segments
	grep "_$lecID" data/train/cmvn.scp > data/test$i/cmvn.scp
	grep "_$lecID" data/train/wav.scp > data/test$i/wav.scp
	cut -d' ' -f1,2 data/test$i/segments > data/test$i/utt2spk
	perl utils/utt2spk_to_spk2utt.pl data/test$i/utt2spk > data/test$i/spk2utt
	steps/make_mfcc.sh --cmd "$train_cmd" --nj $feats_nj data/test$i ;
done
fi


count=10
if [ $stage -le 5 ]; then

echo ============================================================================
echo "                     Decoding                         "
echo ============================================================================

##seq 1 $count |parallel -k "steps/decode.sh --nj 1 exp/mono/graph{1} data/test{1} exp/mono/decode{1}"
##seq 1 $count |parallel -k "steps/decode.sh --nj 1 exp/tri1/graph{1} data/test{1} exp/tri1/decode{1}"
##seq 1 $count |parallel -k "steps/decode.sh --nj 1 exp/tri2/graph{1} data/test{1} exp/tri2/decode{1}"
seq 1 $count |parallel -k "steps/decode_fmllr.sh --nj 1 exp/tri3/graph{1} data/test{1} exp/tri3/decode{1}"
seq 1 $count |parallel -k "./decode_fmllr.sh --nj 1 exp/tri3/graph{1} data/test{1} exp/tri3/decode{1}"



#
#
gunzip exp/tri3_ali/ctm.1.gz
join -1 1 -2 1 data/train/segments exp/tri3_ali/ctm.1 |awk '{print $1" "$3+$6" "$3+$6+$7" "$8}' > exp/ctm1
mkdir -p Data/train
rm Data/text

for i in `seq 1 $count`
	do
	rm -rf Exp/*
	mkdir Exp/tri3
	lecID=`less temp.lst |cut -d' ' -f3 |head -$i |tail -1`
	cp -r exp/tri3/graph$i Exp/tri3/graph

	grep "^108" exp/tri3/decode$i/log/decode.1.log > data/test$i/text
	cp data/test$i/text data/test$i/split1/1/text
	steps/align_fmllr.sh --nj 1 data/test$i data2/lang$i exp/tri3 Exp/tri3_ali
	steps/get_train_ctm.sh data/test$i data2/lang$i Exp/tri3_ali
	gunzip Exp/tri3_ali/ctm.1.gz
	grep "_$lecID" exp/ctm1 |cut -d' ' -f4 |tr '\n' ' ' > Exp/t1
	join -1 1 -2 1 data/test$i/segments Exp/tri3_ali/ctm.1 |awk '{print $1" "$3+$6" "$3+$6+$7" "$8}' > Exp/ctm2
	less Exp/ctm2 >> exp/ctm2
######################	Do this manually with iter.sh
#	cut -d' ' -f4 Exp/ctm2 |tr '\n' ' ' > Exp/t2
#	python align.py Exp/t1 Exp/t2 > Exp/out
#	head -1 Exp/out > Exp/out1
#	head -2 Exp/out |tail -1> Exp/out2
#	awk '{for(i=1;i<=NF;i++){printf "%s %d ", $i,i; if($i!="-"){j++; printf "%d", j};printf "\n"}}' Exp/out2 > Exp/o2
#	awk '{for(i=1;i<=NF;i++){printf "%s %d ", $i,i; if($i!="-"){j++; printf "%d", j};printf "\n"}}' Exp/out1 > Exp/o1
#	less Exp/o2 >> exp/o2
#	less Exp/o1 >> exp/o1
#	join -1 2 -2 2 Exp/o1 Exp/o2 > Exp/o3
#	awk -v j=1 'NR==FNR{id[NR]=$1; text[NR]=$4; next;}{j=$(NF); if(id[j]!=id[j-1]){printf "\n%s ",id[j]}; if(text[j]==$(NF-1) && $2 != "-"){printf "%s ",$2}}' Exp/ctm2 Exp/o3 >> Data/text
done
less Data/text |awk 'NF>=2{print $0}' |sed 's/ / SIL /' |sed 's/$/ SIL/' > Data/train/text
fi
exit
if [ $stage -le 6 ]; then

echo ============================================================================
echo "                     L3 Language Model                      "
echo ============================================================================

#Copy the contents of data/train/text and Data/train/text to data3/train/text

mkdir dump3
rm -rf data3/*
mkdir -p data3/train
mkdir -p data3/local/lm
mkdir -p data3/local/dict
cat data3/train/text |cut -d' ' -f2- |sed 's/ /\n/g' |sort |uniq |sed '1d' > dump3/words
perl /common/software/cmusphinx/logios/Tools/MakeDict/make_pronunciation.pl -tools /common/software/cmusphinx/logios/Tools -dictdir dump3 -words words -dict dict
cp templateDir/* data3/local/dict/
#sed -i '1 i\SIL\tSIL' data3/local/dict/
less digits_lex.txt dump3/dict |sort |awk '{if($1==prev){printf "%s(%d)\t", $1,j; j++}else{printf "%s\t", $1; j=1}; for(i=2;i<=NF;i++){printf "%s ",$i}; printf "\n"; prev=$1}' |sed '1 i\SIL\tSIL' > data3/local/dict/lexicon.txt

less digits_lex.txt dump3/dict |sort |awk '{if($1==prev){printf "%s(%d)\t", $1,j; j++}else{printf "%s\t", $1; j=1}; for(i=2;i<=NF;i++){printf "%s ",$i}; printf "\n"; prev=$1}' |sed '1 i\SIL\tSIL' > data3/local/dict/lexicon.txt

bash utils/prepare_lang.sh --sil-prob 0.0 --position-dependent-phones false --num-sil-states 3 data3/local/dict "SIL" data3/local/lang_tmp data3/lang
bash train_lms_srilm3.sh data3 data3/local/lm_tmp #This take input from data/train/text. Modify this.
compile-lm data3/local/lm_tmp/4gram.me.gz -t=yes /dev/stdout |grep -v UNK | gzip -c > data3/local/lm/lm_phone_bg.arpa.gz
gunzip -c data3/local/lm/lm_phone_bg.arpa.gz | egrep -v '<s> <s>|</s> <s>|</s> </s>' | arpa2fst - | fstprint |utils/eps2disambig.pl |utils/s2eps.pl | fstcompile --isymbols=data3/lang/words.txt --osymbols=data3/lang/words.txt  --keep_isymbols=false --keep_osymbols=false | fstrmepsilon | fstarcsort --sort_type=ilabel > data3/lang/G.fst

#utils/mkgraph.sh data1/lang exp/mono exp/mono/graph$i
#utils/mkgraph.sh data1/lang exp/tri1 exp/tri1/graph$i
#utils/mkgraph.sh data1/lang exp/tri2 exp/tri2/graph$i
#utils/mkgraph.sh data1/lang Exp/tri3 Exp/tri3/graph$i


echo ============================================================================
echo "                     L2 Training                         "
echo ============================================================================

segments=dump1/Segments
cp data/train/wav.scp Data/train/
cp data/train/cmvn.scp Data/train/
join -1 1 -2 1 <(sort -k1 $segments) Data/train/text |cut -d' ' -f1-4 > Data/train/segments
cut -d' ' -f1,2 Data/train/segments > Data/train/utt2spk
perl utils/utt2spk_to_spk2utt.pl Data/train/utt2spk > Data/train/spk2utt

steps/make_mfcc.sh --cmd "$train_cmd" --nj $feats_nj Data/train ;

steps/train_mono.sh --boost-silence 1.25 --nj "$train_nj" Data/train data/lang Exp/mono 
steps/align_si.sh --boost-silence 1.25 --nj "$train_nj" Data/train data/lang Exp/mono Exp/mono_ali
steps/train_deltas.sh --boost-silence 1.25 2000 10000 Data/train data/lang Exp/mono_ali Exp/tri1
steps/align_si.sh --nj "$train_nj"  Data/train data/lang Exp/tri1 Exp/tri1_ali
steps/train_lda_mllt.sh --splice-opts "--left-context=3 --right-context=3" 2500 15000  Data/train data/lang Exp/tri1_ali Exp/tri2
steps/align_si.sh  --nj "$train_nj" Data/train data/lang Exp/tri2 Exp/tri2_ali
steps/train_sat.sh 4200 40000 Data/train data/lang Exp/tri2_ali Exp/tri3
steps/align_fmllr.sh --nj 1 Data/train data/lang Exp/tri3 Exp/tri3_ali

for i in `seq 1 $count`
	do
	#utils/mkgraph.sh data2/lang$i Exp/mono Exp/mono/graph$i
	#utils/mkgraph.sh data2/lang$i Exp/tri1 Exp/tri1/graph$i
	#utils/mkgraph.sh data2/lang$i Exp/tri2 Exp/tri2/graph$i
	utils/mkgraph.sh data3/lang Exp/tri3 Exp/tri3/graph$i
done


for i in `seq 1 $count`
	do
	rm -rf data/Test$i/*
	mkdir -p data/Test$i
	lecID=`less temp.lst |cut -d' ' -f3 |head -$i |tail -1`
	grep "_$lecID" $segments > data/Test$i/segments
	grep "_$lecID" Data/train/cmvn.scp > data/Test$i/cmvn.scp
	grep "_$lecID" Data/train/wav.scp > data/Test$i/wav.scp
	cut -d' ' -f1,2 data/Test$i/segments > data/Test$i/utt2spk
	perl utils/utt2spk_to_spk2utt.pl data/Test$i/utt2spk > data/Test$i/spk2utt
	steps/make_mfcc.sh --cmd "$train_cmd" --nj $feats_nj data/Test$i ;
done


fi

seq 1 $count |parallel -k "steps/decode_fmllr.sh --nj 1 Exp/tri3/graph{1} data/Test{1} Exp/tri3/Decode{1}"
seq 1 $count |parallel -k "decode_fmllr.sh --nj 1 Exp/tri3/graph{1} data/Test{1} Exp/tri3/Decode{1}"

for i in `seq 1 $count`
	do
	grep "^108" Exp/tri3/Decode$i/log/decode.1.log > data/Test$i/text
done

for i in `seq 1 $count`
	do
	mkdir Exp/Tri3
	lecID=`less temp.lst |cut -d' ' -f3 |head -$i |tail -1`
	cp -r exp/tri3/graph$i Exp/Tri3/graph

	grep "^108" exp/tri3/Decode$i/log/decode.1.log > data/Test$i/text
	cp data/Test$i/text data/Test$i/split1/1/text
	steps/align_fmllr.sh --nj 1 data/Test$i data1/lang exp/tri3 Exp/tri3_ali
	steps/get_train_ctm.sh data/test$i data1/lang Exp/tri3_ali
	gunzip Exp/tri3_ali/ctm.1.gz
	grep "_$lecID" exp/ctm1 |cut -d' ' -f4 |tr '\n' ' ' > Exp/t1
	join -1 1 -2 1 data/test$i/segments Exp/tri3_ali/ctm.1 |awk '{print $1" "$3+$6" "$3+$6+$7" "$8}' > Exp/ctm2
done

seq 1 41 |parallel -k "cp -r data/test{1} data/Test{1}"
seq 1 41 |parallel -k "grep \"^108\" Exp/tri3/Decode{1}/log/decode.1.log > data/Test{1}/text"
seq 1 41 |parallel -k "steps/get_train_ctm.sh data/Test{1} data1/lang Exp/Tri3_ali{1}"
#gunzip exp/tri3_ali/ali.1.gz
#ali-to-phones --ctm-output exp/tri3/final.mdl ark:exp/tri3_ali/ali.1 exp/tri3_ali/ctm
#cp exp/tri3_ali/ctm $ctm
#join -1 1 -2 1 $segments $ctm |awk -v line=0 '{if($1!=prev){line++; printf "\n%d\n00:%02d:%02d,%03d --> 00:%02d:%02d,%03d\n",line,$3/60,int($3)%60,($3-int($3))*1000,$4/60,int($4)%60,($4-int($4))*1000}else{printf $8" "};prev=$1}' |sed '1d' > $srt

