#!/bin/bash

unique_words=$1
phone_file_name=$2
rm ${phone_file_name}.cls ${phone_file_name}

while IFS= read -r word; do
        valgrind unified/unified-parser $word 1 0 0 0
        output=`cat wordpronunciation`
        echo $word $output >>${phone_file_name}.cls
        #rm ${word}.wordpronunciation
done < $unique_words

cp ${phone_file_name}.cls ${phone_file_name}
#bash get_phone_mapped_text.sh ${phone_file_name}
