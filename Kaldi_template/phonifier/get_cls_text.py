from argparse import ArgumentParser
from tqdm import tqdm
import numpy as np
import os

parser = ArgumentParser()
parser.add_argument('-o', '--output-file', type=str)
parser.add_argument('-f', '--file-list', nargs='+', default=[])
parser.add_argument('-m', '--cls-mapping', type=str)
args = parser.parse_args()
file =  open(args.cls_mapping, 'r')

data_dict = {}
ind=0
for line in file:
	line=line.strip()
	try:
		key = line.split(" ")[0]
		value = line.split(" ")[1]
		data_dict[key]=value
	except:	
		ind+=1
print("skipped "+str(ind)+" lines in dictionary")
#print(data_dict)
file.close()

text_file = open(args.output_file, "w")


for file_name in args.file_list:
	print("transcribing from "+ file_name)
	file = open(file_name, 'r')
	ind=0
	for line in tqdm(file):
		line = line.strip()
		#line = line.replace('\t', ' ')
		#line = line.replace("'", '')
		#line = line.replace("‘", '')
		#line = line.replace("’", '')
		#line = line.replace('"','')
		#line = line.replace('-',' ')
		#line = line.replace('।','.')
		#line = line.replace('‌','')
		transcription=" ".join(line.split(' '))

		#if ':' in transcription:
		#	transcription=transcription.split(':')[1]
		#print(line.split(' '))
		cls_words=[]
		try:
			for word in transcription.split(" "):
				#print(word)
				if word == "":
					continue
				elif len(word)==1 and word not in data_dict:
					continue
				#elif "." in word:
				#	cls_words.append(".".join([data_dict[x] for x in word.split(".")]))
				#	continue
				#elif '?' == word[-1] or ',' == word[-1]  :
				#	cls_words.append(data_dict[word[:-1]]+word[-1])
				#	continue
				elif word not in data_dict:
					print(word)
				cls_words.append(data_dict[word])
			cls_transcription=" ".join(cls_words)
			text_file.write(cls_transcription+"\n")
		except:
			#print(transcription)
			#input("Press Enter to continue...")
			ind=ind+1
	print("skipped "+str(ind)+" lines")
	file.close()

text_file.close()
