#!/bin/bash
##############################################################################
#										#	
#	created by Mari Ganesh Kumar M					#
#	adapted by Mano Ranjith Kumar M					#	
#										#
#	date : Jul 26 2020							#
#										#
#	email : cs19s032@cse.iitm.ac.in					#
#										#	
##############################################################################
working_dir=$4
stage=-1
textfile=$1
if [ -d ${working_dir}/dump ]; then
    rm -rf ${working_dir}/dump
    rm only_text
    rm lexicon.txt
    rm cls_map
fi
mkdir ${working_dir}/dump
awk '{$1=""; print $0}' $textfile > ${working_dir}/only_text.txt
#@sed -i "s/[[:punct:]]\+//g" ${working_dir}/only_text.txt
python ${working_dir}/get_unique_words.py -f ${working_dir}/only_text.txt -o ${working_dir}/dump/words.txt
#!cat ${working_dir}/only_text.txt | grep -o -E '\w+' | sort -u -f > ${working_dir}/dump/words.txt
unique_words_file="dump/words.txt"
dictionary_file=$2
nj=$3



echo "switching working dir to local/phonify_text"
cd ${working_dir}/phonify_text/

tmpdir=${working_dir}/dump/tmp
if [ $stage -le 0 ]; then
if [ -d "$tmpdir" ]; then
  rm -rf $tmpdir
fi

if [ -f "${working_dir}/${dictionary_file}" ]; then
 rm ${working_dir}/${dictionary_file}
fi
mkdir $tmpdir

echo ${working_dir}
echo ${unique_words_file}

cat ${working_dir}/${unique_words_file} | parallel -k -j $nj "valgrind ./unified-parser {1} $tmpdir/{1}.txt 1 0 0 0"

var1="$(ls $tmpdir/*.txt | wc -l)"
var4="$(less ${working_dir}/${unique_words_file} | wc -l)"

if [ $var1 -ne $var4 ]; then
echo " Unified-parser has not parsed for all words. Check words.txt"
exit 1
fi

#cat wordpronunciation >> unified_parser_output
cp -r word/* ../dump/tmp/ 
fi
if [ $stage -le 1 ]; then
cat ${working_dir}/${unique_words_file} | parallel -k -j $nj "bash ${working_dir}/remove_set_wordstruct.sh $tmpdir/{1}.txt"
while IFS= read -r word; do
	output=`cat $tmpdir/${word}.txt`
	neword=`cat $tmpdir/${word}.txt | sed "s/ //g"`
	echo $word $neword >> ${working_dir}/cls_map
	echo $neword $output >> ${working_dir}/${dictionary_file}
done < ${working_dir}/$unique_words_file	 
echo "switching working dir back to ${working_dir}"
cd ${working_dir}
fi
python get_cls_text.py -o ${working_dir}/output_text.txt -f ${working_dir}/only_text.txt -m ${working_dir}/cls_map
awk '{print $1}' ${working_dir}/${textfile} > ${working_dir}/header
paste -d " " ${working_dir}/header ${working_dir}/output_text.txt > new_text.txt
#cp lexicon_cls lexicon_phone
#bash get_phone_mapped_text.sh
