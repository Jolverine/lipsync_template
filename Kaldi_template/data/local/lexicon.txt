aqt a q t
aqtar a q t a r
aqtim a q t i m
agar a g a r
aglaa a g l aa
aglii a g l ii
acchaa a c ch aa
acchii a c ch ii
ajiib a j ii b
atxhaarah a txh aa r a h
adhik a dh i k
adhikaaqsh a dh i k aa q sh
aniwaarya a n i w aa r y a
anubhwa a n u bh w a
anoupcaarik a n ou p c aa r i k
anya a n y a
apdxeetx a p dx ee tx
apnaa a p n aa
apnee a p n ee
apar a p a r
apeeksxit a p ee k sx i t
apeenxdx a p ee nx dx
ab a b
abhii a bh ii
arithmeetxiklii a r i th m ee tx i k l ii
arth a r th
alag a l a g
alaawaa a l aa w aa
algoritham a l g o r i th a m
algorithmik a l g o r i th m i k
algorithms a l g o r i th m s
awalokan a w a l o k a n
asaain a s aa i n
asiim a s ii m
aseeqdxiqg a s ee q dx i q g
aaiee aa i ee
aaitxam aa i tx a m
aaitxams aa i tx a m s
aaii aa ii
aautx aa u tx
aautxputx aa u tx p u tx
aaeeqgee aa ee q g ee
aaeegaa aa ee g aa
aao aa o
aagee aa g ee
aaj aa j
aatxh aa txh
aataa aa t aa
aatii aa t ii
aatee aa t ee
aadhaar aa dh aa r
aap aa p
aapkaa aa p k aa
aapkee aa p k ee
aapko aa p k o
aam aa m
aamtour aa m t ou r
aargumeeqtx aa r g u m ee q tx
aargyuumeeqtxs aa r g y uu m ee q tx s
aardxar aa r dx a r
aawashyak aa w a sh y a k
aawashyaktaa aa w a sh y a k t aa
aasapaas aa s a p aa s
aasaan aa s aa n
iqgreedxieeqtxs i q g r ee dx i ee q tx s
iqglish i q g l i sh
iqtxramiidxieetx i q tx r a m ii dx i ee tx
iqdxeeqtxeedx i q dx ee q tx ee dx
iqteejras i q t ee j r a s
iqstxrakshaqs i q s tx r a k sh a q s
ikkiis i k k ii s
ikwaalitxii i k w aa l i tx ii
itnaa i t n aa
itnee i t n ee
in i n
inphaarmeeshan i n ph aa r m ee sh a n
inmee i n m ee
inmeeq i n m ee q
insee i n s ee
inheeq i n h ee q
is i s
isakaa i s a k aa
isakee i s a k ee
isameeq i s a m ee q
isaliee i s a l i ee
isii i s ii
isee i s ee
isteemaal i s t ee m aa l
ucc u c c
utxhaaee u txh aa ee
udxqaan u dxq aa n
udxqaanoq u dxq aa n o q
utpaadan u t p aa d a n
udaahranx u d aa h r a nx
un u n
unkaa u n k aa
unkii u n k ii
unkee u n k ee
unheeq u n h ee q
upyog u p y o g
ummiid u m m ii d
us u s
usasee u s a s ee
usii u s ii
usee u s ee
uupar uu p a r
ee ee
eeqdx ee q dx
eek ee k
eeks ee k s
eekseekyuutx ee k s ee k y uu tx
een ee n
eeph ee ph
eem ee m
eemptxii ee m p tx ii
eelimeeqtx ee l i m ee q tx
eelgoridam ee l g o r i d a m
eelgoridmik ee l g o r i d m i k
eewrii ee w r ii
eesaeemaees ee s a ee m a ee s
eidx ei dx
eisaa ei s aa
eisii ei s ii
eisee ei s ee
axpreeshan ax p r ee sh a n
axptximam ax p tx i m a m
axptximaaijeeshan ax p tx i m aa i j ee sh a n
axrdxars ax r dx a r s
axf ax f
opan o p a n
or o r
oupcaarik ou p c aa r i k
our ou r
kaqdxiishan k a q dx ii sh a n
kaqdxiishnal k a q dx ii sh n a l
kaqpyuutx k a q p y uu tx
kaqpyuutxar k a q p y uu tx a r
kaqpyuutxiqg k a q p y uu tx i q g
kaqpyuutxeeshans k a q p y uu tx ee sh a n s
kaii k a ii
kadmoq k a d m o q
kabhii k a bh ii
kam k a m
kamree k a m r ee
kamobeesh k a m o b ee sh
kampyuutx k a m p y uu tx
kampyuutxars k a m p y uu tx a r s
kampyuutxeeshan k a m p y uu tx ee sh a n
kampyuutxeeshnal k a m p y uu tx ee sh n a l
kar k a r
karkee k a r k ee
kartaa k a r t aa
kartii k a r t ii
kartee k a r t ee
karnaa k a r n aa
karnii k a r n ii
karnee k a r n ee
karee k a r ee
kareeq k a r ee q
kareeqgee k a r ee q g ee
kareegaa k a r ee g aa
kaleekshaqs k a l ee k sh a q s
kaleekshan k a l ee k sh a n
kalpanaa k a l p a n aa
kah k a h
kahtaa k a h t aa
kahtee k a h t ee
kahnaa k a h n aa
kahaa k a h aa
kahiiq k a h ii q
kaheeqgee k a h ee q g ee
kaheegaa k a h ee g aa
kaa k aa
kaauqtx k aa u q tx
kaam k aa m
kaarak k aa r a k
kaaranx k aa r a nx
kaarya k aa r y a
ki k i
kiee k i ee
kitnaa k i t n aa
kiyaa k i y aa
kiyee k i y ee
kisii k i s ii
kii k ii
kuch k u ch
kursiyaamq k u r s i y aa mq
kursiyoq k u r s i y o q
kushal k u sh a l
kee k ee
keewal k ee w a l
keisee k ei s ee
kaxman k ax m a n
kaxmaa k ax m aa
kaxl k ax l
kaxlam k ax l a m
kaxstx k ax s tx
ko k o
koii k o ii
kodx k o dx
kolan k o l a n
kolons k o l o n s
koshish k o sh i sh
koun k ou n
kyaa k y aa
kyoq k y o q
kyoqki k y o q k i
kram k r a m
klojdx k l o j dx
kwaaqtxitxii k w aa q tx i tx ii
ksxamtaa k sx a m t aa
khqaasiyat khq aa s i y a t
khaataa kh aa t aa
khaanaa kh aa n aa
khaalii kh aa l ii
khilaaph kh i l aa ph
khud kh u d
kheel kh ee l
kho kh o
khojnaa kh o j n aa
khojnee kh o j n ee
khojeeq kh o j ee q
gaii g a ii
gaee g a ee
gatxhan g a txh a n
ganxit g a nx i t
gayaa g a y aa
gaaraqtxii g aa r a q tx ii
gintee g i n t ee
gucchaa g u c ch aa
gujartee g u j a r t ee
gujree g u j r ee
gunxaa g u nx aa
gunaa g u n aa
guzree g u z r ee
geems g ee m s
greetxeestx g r ee tx ee s tx
catur c a t u r
caltee c a l t ee
calnee c a l n ee
calaanee c a l aa n ee
caliee c a l i ee
caar c aa r
caah c aa h
caahtaa c aa h t aa
caahtee c aa h t ee
caahiee c aa h i ee
citrit c i t r i t
ciijeeq c ii j ee q
ciijoq c ii j o q
cuneeq c u n ee q
cuuqki c uu q k i
ceis c ei s
coudah c ou d a h
chah ch a h
chotxaa ch o tx aa
chotxee ch o tx ee
chodxq ch o dxq
jagah j a g a h
jaghoq j a g h o q
jatxil j a tx i l
jab j a b
jaruurat j a r uu r a t
jawaab j a w aa b
jahaaq j a h aa q
jaa j aa
jaamqc j aa mq c
jaaqc j aa q c
jaaqcataa j aa q c a t aa
jaaee j aa ee
jaaeegaa j aa ee g aa
jaaeegii j aa ee g ii
jaakee j aa k ee
jaataa j aa t aa
jaatee j aa t ee
jaankaarii j aa n k aa r ii
jaantee j aa n t ee
jaannaa j aa n n aa
jaannee j aa n n ee
jaanaa j aa n aa
jaargan j aa r g a n
jaahir j aa h i r
jinkii j i n k ii
jinheeq j i n h ee q
jinhoqnee j i n h o q n ee
jis j i s
jiskaa j i s k aa
jisee j i s ee
jiiwan j ii w a n
jiisiidxii j ii s ii dx ii
jee j ee
jeedx j ee dx
jeisaa j ei s aa
jeisee j ei s ee
jo j o
jodxq j o dxq
jodxqtee j o dxq t ee
jodxqaa j o dxq aa
jodxqii j o dxq ii
jodxqeeq j o dxq ee q
jodxq j o dxq
jodxqtaa j o dxq t aa
txaaip tx aa i p
txaaim tx aa i m
txaask tx aa s k
txiim tx ii m
txuu tx uu
txruu tx r uu
txreiktxeebal tx r ei k tx ee b a l
txhiik txh ii k
txhos txh o s
dxaatxaa dx aa tx aa
dxaal dx aa l
dxaaleeq dx aa l ee q
dxiphaain dx i ph aa i n
dxiwaaidx dx i w aa i dx
dxiwaaidxeedx dx i w aa i dx ee dx
dxiwaaizar dx i w aa i z a r
dxiwaaizars dx i w aa i z a r s
dxiwiizan dx i w ii z a n
dxish dx i sh
dxifaain dx i f aa i n
dxii dx ii
dxeetxaa dx ee tx aa
dxeeph dx ee ph
dxeephinishan dx ee ph i n i sh a n
dxeeskeendxiqg dx ee s k ee n dx i q g
dxaxkyuumeeqtx dx ax k y uu m ee q tx
dxhaqg dxh a q g
dxhuuqdxhatee dxh uu q dxh a t ee
tak t a k
tathya t a th y a
tab t a b
tabhii t a bh ii
tarah t a r a h
tariikaa t a r ii k aa
tariikee t a r ii k ee
tariikoq t a r ii k o q
tark t a r k
taaki t aa k i
tirsatxh t i r s a txh
tiin t ii n
tulnaa t u l n aa
teiyaar t ei y aa r
to t o
tour t ou r
thaa th aa
thee th ee
thodxqaa th o dxq aa
thodxqee th o dxq ee
darshaatee d a r sh aa t ee
darshaanee d a r sh aa n ee
das d a s
daaiiq d aa ii q
daayree d aa y r ee
diee d i ee
dikhaaii d i kh aa ii
din d i n
diwaaidxs d i w aa i dx s
duusraa d uu s r aa
duusrii d uu s r ii
duusree d uu s r ee
drqshyoq d rq sh y o q
deeq d ee q
deeqgee d ee q g ee
deekh d ee kh
deekhtaa d ee kh t aa
deekhtee d ee kh t ee
deekhnaa d ee kh n aa
deekhaa d ee kh aa
deekhee d ee kh ee
deekheeq d ee kh ee q
deekheeqgee d ee kh ee q g ee
deetaa d ee t aa
deetii d ee t ii
deetee d ee t ee
deenaa d ee n aa
deenee d ee n ee
do d o
donoq d o n o q
dohraaee d o h r aa ee
dwaaraa d w aa r aa
dhaarnxaa dh aa r nx aa
dhyaan dh y aa n
na n a
naqbar n a q b a r
naqbaroq n a q b a r o q
naqbars n a q b a r s
naee n a ee
nazar n a z a r
nambar n a m b a r
nayaa n a y aa
nahiiq n a h ii q
naa n aa
naain n aa i n
naamak n aa m a k
nikaal n i k aa l
nikaalaa n i k aa l aa
nirdeesh n i r d ee sh
nirbhar n i r bh a r
nirmaanx n i r m aa nx
nishcit n i sh c i t
nisxkarsx n i sx k a r sx
nee n ee
neem n ee m
neems n ee m s
nou n ou
nyuumeerikal n y uu m ee r i k a l
paqkcueeshan p a q k c u ee sh a n
paqkti p a q k t i
paqktiyoq p a q k t i y o q
pakaanee p a k aa n ee
pacciis p a c c ii s
padxqtaa p a dxq t aa
padxhq p a dxhq
padxhqnee p a dxhq n ee
padxhqaa p a dxhq aa
pataa p a t aa
par p a r
parantu p a r a n t u
parwaah p a r w aa h
parseeqtx p a r s ee q tx
parseeqtxeej p a r s ee q tx ee j
paricit p a r i c i t
paribhaasxaa p a r i bh aa sx aa
paribhaasxit p a r i bh aa sx i t
pariiksxanx p a r ii k sx a nx
paryaapt p a r y aa p t
pahcaan p a h c aa n
pahlaa p a h l aa
pahlii p a h l ii
pahlee p a h l ee
paa p aa
paamqc p aa mq c
paaqc p aa q c
paaithan p aa i th a n
paaeeqgee p aa ee q g ee
paatxhyakram p aa txh y a k r a m
paatee p aa t ee
paaythan p aa y th a n
paar p aa r
paawar p aa w a r
paas p aa s
pichlii p i ch l ii
pichlee p i ch l ee
punaupyog p u n a u p y o g
punargatxhit p u n a r g a txh i t
punarwyawasthit p u n a r w y a w a s th i t
puraanaa p u r aa n aa
puraanee p u r aa n ee
puuchtee p uu ch t ee
puuraa p uu r aa
puurii p uu r ii
peeshkash p ee sh k a sh
paxiqtx p ax i q tx
paxiqtxs p ax i q tx s
paxjitxiwa p ax j i tx i w a
posishan p o s i sh a n
posishans p o s i sh a n s
prakaar p r a k aa r
prakriyaa p r a k r i y aa
pratidin p r a t i d i n
pratipaadan p r a t i p aa d a n
pratiit p r a t ii t
pratyeek p r a t y ee k
prayaas p r a y aa s
prastut p r a s t u t
praapt p r aa p t
prograam p r o g r aa m
prograamiqg p r o g r aa m i q g
prograams p r o g r aa m s
probleems p r o b l ee m s
proseesar p r o s ee s a r
plas p l a s
phaqkshan ph a q k sh a n
phaqkshans ph a q k sh a n s
phaainaaitx ph aa i n aa i tx
phir ph i r
pheeqk ph ee q k
pheiktxar ph ei k tx a r
pheiktxars ph ei k tx a r s
pheiktxarsoq ph ei k tx a r s o q
phon ph o n
bacaa b a c aa
bajaaya b a j aa y a
badxqaa b a dxq aa
badxqii b a dxq ii
badxqee b a dxq ee
badxhqaa b a dxhq aa
bataataa b a t aa t aa
badalnaa b a d a l n aa
baddh b a d dh
banaatii b a n aa t ii
banaatee b a n aa t ee
baraabar b a r aa b a r
balki b a l k i
bashartee b a sh a r t ee
bahut b a h u t
baaii b aa ii
baaiiq b aa ii q
baat b aa t
baatoq b aa t o q
baad b aa d
baar b aa r
baarah b aa r a h
baarii b aa r ii
baaree b aa r ee
biqdu b i q d u
bitx b i tx
binaa b i n aa
bilkul b i l k u l
biltxain b i l tx a i n
bii b ii
biic b ii c
buk b u k
bukiqg b u k i q g
beeshak b ee sh a k
beesik b ee s i k
beehtar b ee h t a r
breikeetx b r ei k ee tx
breikeetxs b r ei k ee tx s
badxqaa b a dxq aa
bhalee bh a l ee
bhaasxaaoq bh aa sx aa o q
bhii bh ii
bholaa bh o l aa
maqc m a q c
matlab m a t l a b
manmaanee m a n m aa n ee
manipyuleetx m a n i p y u l ee tx
mashiin m a sh ii n
mahatwapuurnx m a h a t w a p uu r nx
mahsuus m a h s uu s
maainas m aa i n a s
maamlee m aa m l ee
maarks m aa r k s
minimam m i n i m a m
miltaa m i l t aa
milnaa m i l n aa
mileegaa m i l ee g aa
mukhya m u kh y a
mujhee m u jh ee
mushkil m u sh k i l
muul m uu l
meeq m ee q
meeqtx m ee q tx
meel m ee l
meiq m ei q
meinipuleetx m ei n i p u l ee tx
moujuudaa m ou j uu d aa
yadi y a d i
yah y a h
yahaamq y a h aa mq
yahaaq y a h aa q
yahii y a h ii
yahiiq y a h ii q
yaa y aa
yaatraa y aa t r aa
yaad y aa d
yuuniphaarm y uu n i ph aa r m
yee y ee
yogya y o g y a
rakhnee r a kh n ee
rakheeq r a kh ee q
rahnaa r a h n aa
rahaa r a h aa
rahee r a h ee
raheegaa r a h ee g aa
raaitxmostx r aa i tx m o s tx
rimaaiqdxar r i m aa i q dx a r
rukeeqgee r u k ee q g ee
rutx r u tx
ruup r uu p
reeqj r ee q j
reekhaa r ee kh aa
reesipii r ee s i p ii
laqbaa l a q b aa
laksxya l a k sx y a
lag l a g
lagbhag l a g bh a g
lagaa l a g aa
laain l aa i n
liee l i ee
likh l i kh
likhtee l i kh t ee
likhnaa l i kh n aa
likhnee l i kh n ee
likhaa l i kh aa
likhii l i kh ii
likheeq l i kh ee q
limitx l i m i tx
listx l i s tx
listxs l i s tx s
lee l ee
leeq l ee q
leekin l ee k i n
leetaa l ee t aa
leenaa l ee n aa
leenee l ee n ee
leiqgweej l ei q g w ee j
laxng l ax n g
log l o g
logoq l o g o q
loutxaaeeq l ou tx aa ee q
loutxaanaa l ou tx aa n aa
wan w a n
wanth w a n th
waphaadaar w a ph aa d aa r
wardx w a r dx
warnxan w a r nx a n
warnxit w a r nx i t
wartanii w a r t a n ii
wartamaan w a r t a m aa n
wah w a h
wahii w a h ii
waaii w aa ii
waapas w aa p a s
waalaa w aa l aa
waalee w aa l ee
waastawa w aa s t a w a
wicaar w i c aa r
wibhinn w i bh i n n
wiwranx w i w r a nx
wishisxtx w i sh i sx tx
wisheesx w i sh ee sx
wisheesxajnjataa w i sh ee sx a j nj a t aa
wisheesxtaaeeq w i sh ee sx t aa ee q
wistaar w i s t aa r
wistaarit w i s t aa r i t
wistrqt w i s t rq t
wee w ee
weilyuu w ei l y uu
weilyuuj w ei l y uu j
weilyuuoq w ei l y uu o q
wyakti w y a k t i
wyawasthaa w y a w a s th aa
wyawasthit w y a w a s th i t
wyaakhyaan w y aa kh y aa n
shabd sh a b d
shabdoq sh a b d o q
shart sh a r t
shahar sh a h a r
shaamil sh aa m i l
shaayad sh aa y a d
shiitx sh ii tx
shuruaat sh u r u aa t
shuruu sh u r uu
shuunya sh uu n y a
sheesx sh ee sx
shaxrtxakatx sh ax r tx a k a tx
saqkeet s a q k ee t
saqkhyaa s a q kh y aa
saqkhyaaoq s a q kh y aa o q
saqgrah s a q g r a h
saqtusxtx s a q t u sx tx
saqdarbh s a q d a r bh
saqdeeshoq s a q d ee sh o q
saqpatti s a q p a t t i
saqbaqdh s a q b a q dh
saqbhawa s a q bh a w a
saqbhaawnaaeeq s a q bh aa w n aa ee q
saqbhaawit s a q bh aa w i t
saqshodhit s a q sh o dh i t
saktaa s a k t aa
saktii s a k t ii
saktee s a k t ee
sakuuq s a k uu q
sakeeq s a k ee q
saksxam s a k sx a m
sacmuc s a c m u c
sajaawatx s a j aa w a tx
saphaaii s a ph aa ii
sabsee s a b s ee
sabjeektxiwa s a b j ee k tx i w a
sabhii s a bh ii
samajh s a m a jh
samajhnee s a m a jh n ee
samjhaanee s a m jh aa n ee
samya s a m y a
samaan s a m aa n
samaapt s a m aa p t
samaaroh s a m aa r o h
samuuh s a m uu h
saral s a r a l
sawaal s a w aa l
sahii s a h ii
saaitx s aa i tx
saatxh s aa txh
saat s aa t
saath s aa th
saamaanya s aa m aa n y a
saamuuhik s aa m uu h i k
saaree s aa r ee
siqgal s i q g a l
siqtxeeks s i q tx ee k s
siqtxeiktxik s i q tx ei k tx i k
siqtxeiks s i q tx ei k s
sirph s i r ph
siwaaya s i w aa y a
sii s ii
siikweeqs s ii k w ee q s
siikhnee s ii kh n ee
siikheeqgee s ii kh ee q g ee
sujhaawa s u jh aa w a
sudxoku s u dx o k u
sudhaar s u dh aa r
sunishcit s u n i sh c i t
suwidhaaoq s u w i dh aa o q
suucnaaoq s uu c n aa o q
suuciyoq s uu c i y o q
suuciibaddh s uu c ii b a d dh
see s ee
seetx s ee tx
seel s ee l
saxrtxeedx s ax r tx ee dx
soc s o c
soctee s o c t ee
soubhaagya s ou bh aa g y a
skuul s k uu l
skawaayar s k a w aa y a r
stxaar s tx aa r
stxeetxmeeqtxs s tx ee tx m ee q tx s
stxeep s tx ee p
stxeeps s tx ee p s
stxor s tx o r
stxarakcar s tx a r a k c a r
stxarakcars s tx a r a k c a r s
star s t a r
staroq s t a r o q
sthaapit s th aa p i t
spasxtx s p a sx tx
spareedx s p a r ee dx
slaaidx s l aa i dx
swaagat s w aa g a t
ham h a m
hamnee h a m n ee
hamaaraa h a m aa r aa
hamaarii h a m aa r ii
hamaaree h a m aa r ee
hameeq h a m ee q
hameeshaa h a m ee sh aa
har h a r
hal h a l
haaq h aa q
haaii h aa ii
haath h aa th
haalaaqki h aa l aa q k i
haasil h aa s i l
hissaa h i s s aa
hii h ii
huee h u ee
huuq h uu q
hei h ei
heiq h ei q
haxl h ax l
ho h o
hoq h o q
hoqgee h o q g ee
hogaa h o g aa
hogii h o g ii
hotaa h o t aa
hotii h o t ii
hotee h o t ee
honaa h o n aa
honee h o n ee
