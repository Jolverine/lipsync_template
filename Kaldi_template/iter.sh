id=$1
grep "_$id" exp/ctm1 |cut -d' ' -f4 |tr '\n' ' ' > Exp/t1
grep "_$id" exp/ctm2 |cut -d' ' -f4 |tr '\n' ' ' > Exp/t2
grep "_$id" exp/ctm2 > Exp/ctm2_new
python align.py Exp/t1 Exp/t2 > Exp/out
head -1 Exp/out > Exp/out1
head -2 Exp/out |tail -1> Exp/out2
awk '{for(i=1;i<=NF;i++){printf "%s %d ", $i,i; if($i!="-"){j++; printf "%d", j};printf "\n"}}' Exp/out2 > Exp/o2
awk '{for(i=1;i<=NF;i++){printf "%s %d ", $i,i; if($i!="-"){j++; printf "%d", j};printf "\n"}}' Exp/out1 > Exp/o1
less Exp/o2 >> exp/o2
less Exp/o1 >> exp/o1
join -1 2 -2 2 Exp/o1 Exp/o2 > Exp/o3
awk -v j=1 'NR==FNR{id[NR]=$1; text[NR]=$4; next;}{j=$(NF); if(id[j]!=id[j-1]){printf "\n%s ",id[j]}; if(text[j]==$(NF-1) && $2 != "-"){printf "%s ",$2}}' Exp/ctm2_new Exp/o3 >> Data/text_new

