# Input : Text, minimum length of phrase, syllable list (or) word list, syll(1) or word(2), output file

import sys

textfile=sys.argv[1]
minlen=sys.argv[2]
mylist=sys.argv[3]
syll_or_word=sys.argv[4]
output=sys.argv[5]


with open(textfile,"r") as fp:
	text_lines = fp.read().splitlines()

with open(mylist,"r") as fp:
	tags = fp.read().splitlines()
print(tags)
new_lines = []
dist = 0
if(int(syll_or_word)==1):
	for line in text_lines:
		new_line = []
		words = line.split()
		for i in range(len(words)):
			word = words[i]
			if(word.endswith(tuple(tags)) and dist > int(minlen) and i!=(len(words)-1)):
				new_line.append(word+",")
				dist = 0
			else:
				new_line.append(word)
				dist+=1		
		new_lines.append(" ".join(new_line)+"\n")
		
if(int(syll_or_word)==2):
	for line in text_lines:
		new_line = []
		words = line.split()
		for i in range(len(words)):
			word = words[i]
			if((word in tags) and dist > int(minlen) and i!=(len(words)-1)):
				new_line.append(word+",")
				dist = 0	
			else:
				new_line.append(word)
				dist+=1		
		new_lines.append(" ".join(new_line)+"\n")


with open(output,"w") as fp:
	fp.writelines(new_lines)
	
			
		
