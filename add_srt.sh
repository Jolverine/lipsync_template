#!/bin/bash

final_vid=$1
working_folder=$2
transcription_sub=$3
skip_flag=$4
[ -e output_video.mp4 ] && rm output_video.mp4


rm $final_vid/*.srt
rm $final_vid/*.ass
rm $final_vid/*_wsub.mp4
rm $final_vid/*_wa.mp4

ls  $final_vid/srt_seg*.mp4 |parallel -k "ffmpeg -nostdin -i {1} 2>&1|grep Duration |awk '{print \$2}' |sed 's/,//' |sed 's/\./,/' |awk '{print 1; print\"00:00:00,000 --> \"\$1}' > {1.}.srt" #dummy SRT files are created for each mp4 segments
ls -v $final_vid/*.srt |awk '{print NR" "$0}' > $working_folder/V4_template/temp_list/srt.lst
less $working_folder/V4_template/temp_list/srt.lst |parallel -k --colsep ' ' "awk -v n={1} 'NR==n{print \$0}' $3 >> {2}"
ls $final_vid/*.srt |parallel -k "ffmpeg -nostdin -i {1} {1.}.ass"

find $final_vid/ -type f -name "*.ass" -exec sed -i -e 's/Arial,16/Arial,10/g' {} \; # Reduce the font size in subtitle

ls $final_vid/*.ass |parallel -k "ffmpeg -nostdin -i {1.}.mp4 -qscale 0 -vf ass={1} {1.}_wsub.mp4"

rev $working_folder/V4_template/temp_list/srt_full_video_list.txt | cut -d'/' -f1 | rev | sed "s:^:file ':g" | sed "s:$:':g" > $final_vid/final_merge_list.txt

sed -i "/^file 'srt/s/.mp4/_wsub.mp4/g" $final_vid/final_merge_list.txt

FILE=$final_vid/intersrt_seg0000.mp4
if [ -f "$FILE" ]; then
    skip_flag=1
else 
    echo "$FILE does not exist."
fi

if [ $skip_flag == 1 ]
then
	grep -q "$intersrt_seg0000" "$final_vid/final_merge_list.txt" && head -n 1 $final_vid/final_merge_list.txt | cut -d  ' ' -f2 | tr -d \'\" | parallel -k "ffmpeg -nostdin -f lavfi -i anullsrc=channel_layout=stereo:sample_rate=44100 -i $final_vid/{1} -c:v copy -c:a aac -shortest $final_vid/{1.}_wa.mp4"  ## This portion will add the intro clip back

	grep -q "$intersrt_seg0000" "$final_vid/final_merge_list.txt" && sed -i '1 s/.mp4/_wa.mp4/' $final_vid/final_merge_list.txt
else
		echo "skip-flag not set"
fi

ffmpeg -nostdin -f concat -i $final_vid/final_merge_list.txt -fflags +genpts output_video.mp4

